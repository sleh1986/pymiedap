# This file is part of PyMieDAP, released under GNU General Public License.
# See license.md or http://gitlab.com/loic.cg.rossi/pymiedap for details.

"""
# PYthon MIE DAP (PYMIEDAP)
# This code is used to make computations of Mie scattering along with radiative
# transfer calculations with polarization.
# Dependencies : numpy, matplotlib and scipy
# module_mie, module_mieshell, module_readmie, module_dap, module_geos
#
# Authors : Loic Rossi, Daphne Stam
# Date : 2013 - 2016
# Licence for the Python elements: GNU/GPL & CeCILL
# http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
# http://www.gnu.org/copyleft/gpl.html
#
# If you use this code, please refer to
# de Rooij et al. 1984, A&A
# de Haan et al. 1987, A&A
# Stam et al. 2006, A&A
"""

# ==============
# IMPORT MODULES
# ==============
import numpy as np
import numpy.random as npr
import module_mie as mie
import module_mieshell as mieshell
import module_readmie as readmie
import module_dap as dap
import module_geos as geos
import os
import sys
import os.path
import matplotlib.pyplot as mpl
from PIL import Image

# ---------
# CLASSES DEFINITION
# ---------

class Layer():
    """ This class is intended to describe a layer for the Doubling-Adding
    program. It contains the basic parameters of the model

    Parameters
    ----------
    tau : array
        optical thickness (for each lambda)
    tau_g : array
        optical thickness related to gaseous absorption (for each lambda)
    tau_ray : array
        optical thickness related to rayleigh scattering set by user (for each lambda)
    rayscat: Boolean
        if True, rayleigh scattering is computed. If false, tau_ray is used instead.
    press : float
        pressure at the bottom of the layer [bars]
    aerosols :
        an object containing the properties of a type of aerosols.
        Several of these aerosol objects can coexist in a layer, but they should have
        different names.
    col_dens: float
        particular column density in particles per square micrometers
    """

    def __init__(self, tau=[30], tau_g=[0.], press=30e-3, psd='2',
                 mix_factor=0., bmsca=[0], bmabs=[0],
                 tau_ray=[0.], rayscat=True,
                 basca=[0], baabs=[0]):
        """ Initializes the model object with default values
        aerosols: a subclass to describe the properties of the aerosols
        """
        self.aerosols = Aerosols()
        self.tau = tau
        self.tau_g = tau_g
        self.tau_ray = tau_ray
        self.rayscat = rayscat
        self.press = press
        self.col_dens = 3.2
        self.bmsca = bmsca
        self.bmabs = bmabs
        self.basca = basca
        self.baabs = baabs

    def update_layer(self, nitems):
        """ If the number of working wavelengths is changed, this method
        updates the vectors that depend on wavelength
        """
        self.tau = self.tau[0] * np.ones(nitems)
        self.tau_g = self.tau_g[0] * np.ones(nitems)
        self.tau_ray = self.tau_ray[0] * np.ones(nitems)
        self.bmsca = self.bmsca[0] * np.ones(nitems)
        self.bmabs = self.bmabs[0] * np.ones(nitems)
        self.basca = self.basca[0] * np.ones(nitems)
        self.baabs = self.baabs[0] * np.ones(nitems)

    def mix_aerosols(self):
        """ Mixes the aerosols by combining their scattering matrices"""
        # Checking if there is already a mixed aerosol object
        if hasattr(self,'mixed_aerosols') is True:
            del(self.mixed_aerosols)

        # Preparing some variables
        sum_f = 0
        sum_fsext = np.zeros(len(self.tau)) #sum of sext*f
        sum_fssca = np.zeros(len(self.tau)) #sum of ssca*f
        max_ncoefs = 0
        sum_tau_sca = np.zeros(len(self.tau)) #preparing sums

        sum_coefs = 0
        typ = ''

        # normalise the factors
        for aero_name, aero in vars(self).items():
            if isinstance(aero, Aerosols):
                sum_f += aero.f
                max_ncoefs = np.maximum(max_ncoefs, aero.ncoefs)
                typ = typ + aero.typ

        # retrieve the total column density
        for aero_name, aero in vars(self).items():
            if isinstance(aero, Aerosols):
                aero.f = aero.f / float(sum_f)
                sum_fsext += aero.f * aero.sext
                sum_fssca += aero.f * aero.ssca
        N = self.tau[0] / sum_fsext[0]

        # use it to combine coefs
        for aero_name, aero in vars(self).items():
            if isinstance(aero, Aerosols):
                sum_tau_sca += aero.f * aero.ssca

        for aero_name, aero in vars(self).items():
            if isinstance(aero, Aerosols):
                sum_coefs += (aero.f * aero.coefs)
        mix_coefs = sum_coefs #/ sum_tau_sca[:,np.newaxis, np.newaxis, np.newaxis]

        # filling the mixed object with the result
        self.mixed_aerosols = Aerosols()
        self.mixed_aerosols.coefs = mix_coefs
        self.mixed_aerosols.ncoefs = max_ncoefs
        self.mixed_aerosols.typ = typ
        self.mixed_aerosols.col_dens = N
        self.mixed_aerosols.sext = sum_fsext
        self.mixed_aerosols.ssca = sum_fssca
        self.mixed_aerosols.ssalb = sum_fssca/sum_fsext
        #Warning! Might not be okay!
        #self.mixed_aerosols.nr = aero.nr
        #self.mixed_aerosols.ni = aero.ni
        #self.mixed_aerosols.nr_core= aero.nr_core
        #self.mixed_aerosols.ni_core= aero.ni_core

        print("Aerosols mixed!")


class Layers:
    """ This class contains layer objects describing the atmospheric
    structure desired"""

    def __init__(self):

        self.gastop = Layer(tau=[0.0], press=1e-5)
        self.haze = Layer(press=10e-3, tau=[0.01])
        self.cloud = Layer(press=1., psd='3')
        self.gasbelow = Layer(tau=[0.0], press=100)


class Model(object):
    """ This class defines a model planet.

    Parameters
    ----------
    Layers : Layers class object
        contains several layers
    wvl_list : array
        list of wvl to compute. Each time the list is changed (all at
        once) the others vectors within the layers are updated.
    gravity : float
        value of the acceleration of gravity (in m/s^2)
        default 9.81
    asurf : float
        surface albedo for Lambertian surface. If changed, surface[0,0] changes
        too (see below).
        Default is 0
    mma : float
        mean molecular mass of the gas (in atomic mass units)
    dpol : float
        depolarisation factor for the gas medium
        default is 0.09
    rindex_gas : array
        refractive index of the gas (same length as wvl_list). Default is for air.
    Ts : float
        temperature of the star in K
    Rs : float
        Radius of the star in meters
    Dps : float
        distance planet-star in meters
    surface : 4x4 array or string
        an array describing a constant reflection matrix or a filename
        with the Fourier coefficients of a more complicated surface. If surface
        is updated, asurf changes accordingly.

    Returns
    -------
    I, Q, U, V : nd arrays
        Stokes elements
    P : nd array
        degree of linear polarization (-Q/I)
    Pl : nd array
        total degree of linear polarization (sqrt(Q**2+U**2)/I)
    Pt : nd array
        total degree of polarization
    Pu : nd array
        U/I
    Pv : nd array
        V/I
    Pxstd : ndarray
        Standard deviation of quantity x in case of variable pixel masks
    Pxmax1s : ndarray
        Standard deviation of quantity x in case of variable pixel masks
    Pxmax : ndarray
        Max of quantity x in case of variable pixel masks
    Pxmin : ndarray
        Min of quantity x in case of variable pixel masks
    PxminNs : ndarray
        Value of quantity x - N times the standard deviation in case of
        variable pixel masks
    PxmaxNs : ndarray
        Value of quantity x + N times the standard deviation in case of
        variable pixel masks
    fcloud : float
        cloud cover when doing resolved and integrated simulations
    asym : float
        asymmetry of the planet
    picture : 2d array
        image of cloud cover

    The returned attributes depend on the function used on the model object

    """

    def __init__(self, wvl_list=np.array([1.101]), gravity=9.81, dpol=0.09,
                 mma=44, I=[], Q=[], U=[], V=[], P=[], phase=[], asurf=0,
                 Ts=5750, Rs =696342000.,
                 Dps=108208930000.0, rindex_gas=np.array([0]),
                 fcloud=1., asym=0., picture=[], surface=[]):
        """ Generic properties of the model """
        self.gravity = gravity
        self.dpol = dpol
        self.mma = mma
        self.Ts = Ts
        self.Rs = Rs
        self.Dps = Dps

        self.layers = Layers()
        self.geom = Geom()
        self.phase = phase

        self.I = I
        self.Q = Q
        self.U = U
        self.V = V
        self.P = P  # ! warning P is -Q/I
        self._wvl_list = wvl_list
        self.set_rindex_gas()

        self.fcloud = fcloud
        self.asym = asym
        self.picture = picture

        self._surface = np.diag([asurf,0.,0.,0.])
        self._asurf = asurf #surface albedo for lambertian surf

        self.name = ['']

    # auto update of wvl list in layers
    @property
    def wvl_list(self):
        return self._wvl_list

    @wvl_list.setter
    def wvl_list(self, wlist):
        self._wvl_list = wlist
        self.set_rindex_gas()
        for layername,layer in vars(self.layers).items():
            layer.update_layer(len(wlist))
            for aero_name, aero in vars(layer).items():
                if isinstance(aero, Aerosols):
                    aero.update_arrays(len(wlist))

    # mutual update of asurf and surface matrix
    @property
    def asurf(self):
        return self._asurf

    @asurf.setter
    def asurf(self, alb):
        self._asurf = alb
        self._surface[0,0] = self._asurf

    @property
    def surface(self):
        return self._surface

    @surface.setter
    def surface(self, val):
        self._surface = val
        self._asurf = self._surface[0,0]



    def __repr__(self):
        """ Custom display of basic model parameters"""

        strfin = ''
        print('Model:')
        wvl_str = ('**Operating wavelengths:\n'+
                    str(self.wvl_list) + ' microns\n\n')
        planet_str = ("**Planet data:**\n "+
                      "g={:2.2f} m/s^2; ".format(self.gravity) +
                      "surf.alb.={:2.2f}\n \n".format(self.asurf))
        gas_str = ("**Gas data**\n mma={:2.2f} ".format(self.mma) +
                   "dpol={:2.2f}\n".format(self.dpol))
        lays_str = '\n **Layers** \n'
        for layer_name, layer in vars(self.layers).items():
            if hasattr(layer,'mixed_aerosols') is True:
                strout = ('LAYER ' + str(layer_name) +'\n'+
                          ' Type:' + layer.mixed_aerosols.typ +
                          ', P=' + str(layer.press) +
                          ', tau=' + str(layer.tau) +
                          ', tau_gas=' + str(layer.tau_g) + '\n')
                strout += layer.aerosols.__repr__()
            else:
                strout = ('LAYER '+str(layer_name) +'\n'+
                          ' Type:' + layer.aerosols.typ +
                          ', P=' + str(layer.press) +
                          ', tau=' + str(layer.tau) +
                          ', tau_gas=' + str(layer.tau_g) + '\n')
                strout += layer.aerosols.__repr__()
            strfin = strfin + strout
        return wvl_str+planet_str+gas_str+lays_str+strfin


    def model_atm(self,  H=5., z_top=74., k_max=2., r_c = 1.0, v_c = 0.07,
                  n_c=1.42, profile='vertical_profile_ignatiev_38lays.htp',path='./spicavpol/'):
        """New model of Venus'atmosphere with 37 layers for more precise description
        H : scale height
        z_top : height of the top of clouds
        k_max : maximum value of k_ext
        z : height
        dz : thickness of a layer
        z_int : height at the middle of a layer
        z_cut : height below which k_ext is constant
        k_extA : extinction coefficient for z_int >= z_cut
        k_0 : extinction coefficient for z_int <= 47 km
        k_ext : extinction coefficient forall z
        tau_cloud : optical thickness of cloud
        T : temperature
        P : pression
        """

        wvl = self.wvl_list[0]
        nwav = len(self.wvl_list)

        #Reading the file and stocking the data in an array
        tab = np.genfromtxt(path+profile,skip_header=1)

        #Computing the interesting variables
        z = tab[:,0]
        dz = np.diff(z)
        z_int = z[:-1] + dz/2.
        z_cut = z_top - H*np.log(H*k_max)

        k_extA = (1/H) * np.exp(-(z_int-z_top)/H)
        k_0 = np.zeros(len(z_int))
        k_ext = ( (z_int>=z_cut)*k_extA
                 + (z_int<z_cut)*(z_int>47.)*k_max
                 + (z_int<=47.)*k_0 )
						#for z_int >= z_cut        : k_ext = k_extA (mask)
						#for 47 km < z_int < z_cut : k_ext = k_max  (mask)
						#for z_int <= 47 km        : k_ext = 0 km-1 (no cloud below) (mask)
        tau_cloud = k_ext*dz
        T = tab[:,1]
        P = tab[:,2] / 1e3   #pressures in bars!

        #Deleting the old model's layers
        del self.layers.gastop
        del self.layers.haze
        #del self.layers.cloudtop
        del self.layers.cloud
        del self.layers.gasbelow

        #Loading tau_g data and stocking them in a list
        T = []

        for i in np.arange(37):
            t = np.load(path+'dtau_LW/dtau_{:03d}_LW.npz'.format(i))
            T.append(t)

        tab_tau_c = np.zeros((37, nwav))
        tab_tau_g = np.zeros((37, nwav))
        for z,w in enumerate(self.wvl_list):
            # fill table of cloud opacities
            tab_tau_c[:,z] = tau_cloud
            # test: are we in the band?
            if (w<T[0]['lam0'][0]) or (w>T[0]['lam0'][-1]):
                tab_tau_g[:,z] = 0. # if not gaz opacity=0
            else:
                #else give each layer the gaz opacity of the correct wvl
                for l in np.arange(37):
                    index_wvl = np.where(abs(T[l]['lam0']-w) == np.nanmin(abs(T[l]['lam0']-w)))[0]
                    print(index_wvl)
                    tab_tau_g[l,z] = T[l]['tau0'][index_wvl]


        self.layers.layer0 = Layer(tau=tab_tau_c[0,:], tau_g=tab_tau_g[0,:], press=P[0], level=1, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer1 = Layer(tau=tab_tau_c[1,:], tau_g=tab_tau_g[1,:], press=P[1], level=2, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav),typ='C')
        self.layers.layer2 = Layer(tau=tab_tau_c[2,:], tau_g=tab_tau_g[2,:], press=P[2], level=3, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer3 = Layer(tau=tab_tau_c[3,:], tau_g=tab_tau_g[3,:], press=P[3], level=4, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer4 = Layer(tau=tab_tau_c[4,:], tau_g=tab_tau_g[4,:], press=P[4], level=5, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer5 = Layer(tau=tab_tau_c[5,:], tau_g=tab_tau_g[5,:], press=P[5], level=6, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer6 = Layer(tau=tab_tau_c[6,:], tau_g=tab_tau_g[6,:], press=P[6], level=7, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer7 = Layer(tau=tab_tau_c[7,:], tau_g=tab_tau_g[7,:], press=P[7], level=8, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer8 = Layer(tau=tab_tau_c[8,:], tau_g=tab_tau_g[8,:], press=P[8], level=9, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer9 = Layer(tau=tab_tau_c[9,:], tau_g=tab_tau_g[9,:], press=P[9], level=10, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer10 = Layer(tau=tab_tau_c[10,:], tau_g=tab_tau_g[10,:], press=P[10], level=11, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer11 = Layer(tau=tab_tau_c[11,:], tau_g=tab_tau_g[11,:], press=P[11], level=12, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer12 = Layer(tau=tab_tau_c[12,:], tau_g=tab_tau_g[12,:], press=P[12], level=13, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer13 = Layer(tau=tab_tau_c[13,:], tau_g=tab_tau_g[13,:], press=P[13], level=14, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer14 = Layer(tau=tab_tau_c[14,:], tau_g=tab_tau_g[14,:], press=P[14], level=15, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer15 = Layer(tau=tab_tau_c[15,:], tau_g=tab_tau_g[15,:], press=P[15], level=16, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer16 = Layer(tau=tab_tau_c[16,:], tau_g=tab_tau_g[16,:], press=P[16], level=17, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer17 = Layer(tau=tab_tau_c[17,:], tau_g=tab_tau_g[17,:], press=P[17], level=18, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer18 = Layer(tau=tab_tau_c[18,:], tau_g=tab_tau_g[18,:], press=P[18], level=19, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer19 = Layer(tau=tab_tau_c[19,:], tau_g=tab_tau_g[19,:], press=P[19], level=20, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer20 = Layer(tau=tab_tau_c[20,:], tau_g=tab_tau_g[20,:], press=P[20], level=21, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer21 = Layer(tau=tab_tau_c[21,:], tau_g=tab_tau_g[21,:], press=P[21], level=22, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer22 = Layer(tau=tab_tau_c[22,:], tau_g=tab_tau_g[22,:], press=P[22], level=23, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer23 = Layer(tau=tab_tau_c[23,:], tau_g=tab_tau_g[23,:], press=P[23], level=24, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer24 = Layer(tau=tab_tau_c[24,:], tau_g=tab_tau_g[24,:], press=P[24], level=25, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer25 = Layer(tau=tab_tau_c[25,:], tau_g=tab_tau_g[25,:], press=P[25], level=26, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer26 = Layer(tau=tab_tau_c[26,:], tau_g=tab_tau_g[26,:], press=P[26], level=27, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer27 = Layer(tau=tab_tau_c[27,:], tau_g=tab_tau_g[27,:], press=P[27], level=28, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer28 = Layer(tau=tab_tau_c[28,:], tau_g=tab_tau_g[28,:], press=P[28], level=29, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer29 = Layer(tau=tab_tau_c[29,:], tau_g=tab_tau_g[29,:], press=P[29], level=30, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer30 = Layer(tau=tab_tau_c[30,:], tau_g=tab_tau_g[30,:], press=P[30], level=31, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer31 = Layer(tau=tab_tau_c[31,:], tau_g=tab_tau_g[31,:], press=P[31], level=32, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer32 = Layer(tau=tab_tau_c[32,:], tau_g=tab_tau_g[32,:], press=P[32], level=33, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer33 = Layer(tau=tab_tau_c[33,:], tau_g=tab_tau_g[33,:], press=P[33], level=34, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer34 = Layer(tau=tab_tau_c[34,:], tau_g=tab_tau_g[34,:], press=P[34], level=35, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer35 = Layer(tau=tab_tau_c[35,:], tau_g=tab_tau_g[35,:], press=P[35], level=36, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')
        self.layers.layer36 = Layer(tau=tab_tau_c[36,:], tau_g=tab_tau_g[36,:], press=P[36], level=37, r_eff=r_c, v_eff=v_c, nr=n_c*np.ones(nwav), ni=1e-8*np.ones(nwav), typ='C')

        return z_int, k_ext, tau_cloud, tab_tau_g


    def set_taus(self):
        """ modifies the values of the opacity vector according the column
        density and the scattering cross section
        """
        for layername, layer in vars(self.layers).items():
            layer.tau = layer.col_dens * layer.mixed_aerosols.sext
            # should it be sigma_ext or sigma_sca ?

    def set_nrs(self,slope,k):
        """ this function computes the refractive indices as a function of wvl
        given a slope and a constant as in the following equation:
            n = k + slope*lambda
        """

        wvl = np.array(self.wvl_list)
        nrs = k + slope*wvl
        for layername, layer in vars(self.layers).items():
            layer.nr = nrs

    def set_rindex_gas(self, gas='air'):
        """ Computes the refractive index of a gas and sets the refractive
        indices of the model
        INPUTS:
            wvl: wavelength (in microns)
            gas: choose between 'air', 'CO2', 'N2', 'He' and 'H2'
        """
        S = 1./np.array(self.wvl_list)

        if gas=='air':
            # see Ciddor et al. 1996
            rindex = 1 + (0.05792105/(238.0185 - S**2)) + (0.00167917/(57.362 - S**2))
        if gas=='CO2':
            # see Bideau-Mehu el. 1973 (0.1807 -- 1.69 um)
            rindex = 1 + ((6.99100*1e-2/(166.175 - S**2)) + (1.44720*1e-3/(79.609 - S**2)) +
                    (6.42941*1e-5/(56.3064 - S**2)) + (5.21306*1e-5/(46.0196 - S**2))
                    + (1.46847*1e-6/(0.0584738 - S**2)))
        if gas=='H2':
            # Peck and Hung 1977 (0.168 -- 1.6945 um)
            rindex = 1 + (0.0148956/(180.7-S**2)) + (0.0049037/(92 - S**2))
        if gas=='He':
            # Mansfield and Peck 1969
            rindex = 1 + (0.01470091/(423.98 - S**2))
        if gas=='N2':
            # Peck and Khanna 1966 (0.47 -- 2.06 um)
            rindex = 1 + 6.8552*1e-5 + (3.243157*1e-2/(144-S**2))

        self.rindex_gas = rindex


    def summary(self):
        """Writes a summary of model parameters in a .info file with name of
        current model"""

        if self.name==['']:
            print('Model not titled yet!')
        else:
            for w,wvl in enumerate(self.wvl_list):
                filename = self.name[w].split('/')[-1]
                filename = filename.split('.')[:-1]
                filename = '.'.join(filename)
                filename += '.info'
                fich = open(filename,'w')

                strfin = ''
                wvl_str = ('{}:\n'+
                           '**Operating wavelengths:\n'+
                        str(self.wvl_list) + ' microns\n\n')
                planet_str = ("**Planet data:**\n "+
                            "g={:2.2f} m/s^2; ".format(self.gravity) +
                            "surf.alb.={:2.2f}\n \n".format(self.asurf))
                gas_str = ("**Gas data**\n mma={:2.2f} ".format(self.mma) +
                        "dpol={:2.2f}\n".format(self.dpol))
                lays_str = '\n **Layers** \n'
                fich.write(wvl_str)
                fich.write(planet_str)
                fich.write(gas_str)
                fich.write(lays_str)

                for layer_name, layer in vars(self.layers).items():
                    if hasattr(layer,'mixed_aerosols') is True:
                        strout = ('LAYER '+str(layer_name) +'\n'+
                                ' Type:' + layer.mixed_aerosols.typ +
                                ', P=' + str(layer.press) +
                                ', tau=' + str(layer.tau) +
                                ', tau_gas=' + str(layer.tau_g) + '\n')
                        strout += layer.aerosols.__repr__()
                        strfin = strfin + strout
                    else:
                        strout = ('LAYER '+str(layer_name) +'\n'+
                                ' Type:' + layer.aerosols.typ +
                                ', P=' + str(layer.press) +
                                ', tau=' + str(layer.tau) +
                                ', tau_gas=' + str(layer.tau_g) + '\n')
                        strout += layer.aerosols.__repr__()
                        strfin = strfin + strout
                fich.write(strfin)


class Geom():
    """ subclass containing geometrical informations
    Geom : a class containing all the geometrical informations
    sza : solar zenith angle, obtained from data
    emission : emission angle
    azimuth : azimtuh angle
    latitude: latitude
    longitude: longitude
    local_time
    phase : phase angle
    beta : rotation angle btw local meridian and scattering plane (see Hovenier
    1969)
    """

    def __init__(self, sza=[], emission=[],
                 azimuth=[], phase=[], beta=[],
                 latitude=[], longitude=[], local_time=[]):
        self.sza = sza
        self.emission = emission
        self.azimuth = azimuth
        self.phase = phase
        self.beta = beta
        self.latitude = latitude
        self.longitude = longitude
        self.local_time = local_time

    def calc_phase(self):
        """ Computes the phase angle (in degrees) from azimuth, SZA and
        emission angle (also in degrees)"""
        rsza = np.radians(self.sza)
        remission = np.radians(self.emission)
        razimuth = np.radians(self.azimuth)

        cospha = np.cos(rsza)*np.cos(remission) + np.sin(rsza)*np.sin(remission)*np.cos(razimuth)
        rphase = np.arccos(cospha)
        phase = np.degrees(rphase)

        self.phase = phase

    def calc_azimuth(self, deg=True):
        """This method computes the azimuth angle from the geometric
        data. To be used once all geo data have been read and treated.
        INPUTS:
            phase : phase angle
            sza : solar zenith angle
            emission : emission angle
        OUTPUT:
            the azimuthal angle
            If deg==1, the output is given in degrees
            Radians otherwise
        """
        sza = self.sza
        emission = self.emission
        phase = self.phase

        theta = np.radians(sza)
        thetap = np.radians(emission)
        alpha = np.radians(phase)

        t1 = np.cos(alpha) - (np.cos(theta)*np.cos(thetap))
        t2 = np.sin(theta)*np.sin(thetap)
        c_delta_phi = t1/t2
        c_delta_phi[t2<1e-6] = 1. # if denominator is too small, set cos(phi) to 1.
        c_delta_phi[c_delta_phi>1.] = 1.
        c_delta_phi[c_delta_phi<-1.] = -1.

        if deg==1:
            delta_phi = np.degrees(np.arccos(c_delta_phi))
            azimuth = 180. - delta_phi
            azimuth[azimuth<1e-5] = 0.
        else:
            delta_phi = np.arccos(c_delta_phi)
            azimuth = np.pi - delta_phi
            azimuth[azimuth<1e-5] = 0.

        self.azimuth = azimuth

    def calc_beta(self):
        ''' Calculates the rotation angle between the local meridian plane and the
        scattering plane. (see eqs. (7) and (8) in Hovenier 1969)
        Inputs : SZA (deg)
                EMI (deg)
                PHA (deg)
                AZI (deg)
        Returns: BETA (deg)'''
        SZA = self.sza
        EMI = self.emission
        PHA = self.phase
        AZI = self.azimuth

        sgn = np.ones(len(AZI))
        #sgn[AZI<0.] = 1
        #sgn[AZI>0.] = -1

        num = np.cos(np.pi*SZA/180.)-np.cos(np.pi*EMI/180.)*np.cos(np.pi*PHA/180.)
        denom = (sgn*np.sin(np.pi*EMI/180.)*np.sin(np.pi*PHA/180.))
        cb = num/denom
        cb[denom==0] = 0.
        cb[cb>1.] = 1.
        cb[cb<0.] = 0.

        self.beta = np.degrees(np.arccos(cb))


class Aerosols():
    """subclass containing aerosols properties
    nr, ni : real, imaginary parts of refractive index (for each wvl)
    nr_core, ni_core : real, imaginary parts of refractive index for the inner
        core in case of layered spheres (for each wvl)
    rcoremant: ratio between the radius of the outer sphere and the inner core;
        only relevant for layered spheres
    r_eff, v_eff : effective radius and variance (constant with lambda)
    par3: value that can be used for some size distributions
    typ : string indicating type of aerosols ex:'C' for clouds, 'H' for hazes;
        just for user's reference, doesn't change the results in any way
    layered: if True, the calculation is made with Mie scattering for layered
        sphere (see Bohren and Huffmann)
    psd : type of particle size distribution fct 2 stands for modified gamma
    f: is the mix ratio of this type of aerosol in the layer. If f=0.5, half the
    particules are this type.
    * OUTPUT variables:
    qext : extinction coefficients (for each wvl)
    sext : extinction cross-section (for each wvl)
    qsca : scattering coefficients (for each wvl)
    ssca : scattering cross-section (for each wvl)
    coefs: array containing the expansion corefficients from the single
        scattering. For the combined aerosols and for each wvl.

    * SIZE Distributions (value of psd):
        par1 refers to Aerosols.reff
        par2 refers to Aerosols.veff
        par3 refers to Aerosols.par3

        1: TWO PARAMETER GAMMA with alpha (par1) and b (par2) given
        2: TWO PARAMETER GAMMA with reff (par1) and veff (par2) given
        3: Bimodal gamma with equal mode weights
        4: Log normal with rg (par1) and sigma (par2) given
        5: Log normal with reff (par1) and veff (par2) given
        6: Power law with alpha (par1), rmin (par2), rmax(par3)
        7: MODIFIED GAMMA with alpha (par1), rc (par2) and gamma (par3) given
        8: MODIFIED GAMMA with alpha (par1), b (par2) and gamma (par3) given

    """
    def __init__(self, r_eff=1.05, v_eff=0.07, par3=1., rcoremant=0.1, nr=[1.42, 1.41],
                 ni=[1e-8, 1e-8], qext=[0.1, 0.1], sext=[0.1,0.1],
                 ni_core=[1e-8, 1e-8], nr_core=[1.42, 1.41],
                 qsca=[0.1, 0.1], ssca=[0.1,0.1],
                 col_dens=0.6, typ='C', psd='2', layered=False):
        """ Initializes the aerosols object with default values"""
        self.r_eff = r_eff
        self.v_eff = v_eff
        self.par3 = par3
        self.nr = nr
        self.ni = ni
        self.nr_core = nr_core
        self.ni_core = ni_core
        self.rcoremant = rcoremant
        self.typ = typ
        self.psd = psd
        self.qext = qext
        self.sext = sext
        self.qsca = qsca
        self.ssca = ssca
        self.col_dens = col_dens
        self.coefs = []
        self.ncoefs = []
        self.f = 1
        self.layered = layered

    def update_arrays(self, nitems):
        """ adds a new item to each list linked to wvl"""
        self.nr = self.nr[0] * np.ones(nitems)
        self.ni = self.ni[0] * np.ones(nitems)
        self.nr_core = self.nr_core[0] * np.ones(nitems)
        self.ni_core = self.ni_core[0] * np.ones(nitems)
        self.qext = self.qext[0] * np.ones(nitems)
        self.sext = self.sext[0] * np.ones(nitems)
        self.qsca = self.qsca[0] * np.ones(nitems)
        self.ssca = self.ssca[0] * np.ones(nitems)


    def __repr__(self):
        """ Displayed informations when the object is printed """

        if self.layered is True:
            str0 = "Layered spherical particles\n"
            strA = ("nr_core =" + str(self.nr_core) + " )) nr_mantle = "
                    + str(self.nr) + "))\n")
            strB = ("ni_core =" + str(self.ni_core) + " )) ni_mantle = "
                    + str(self.ni) + "))\n")
            strC = ("   R_eff = {:1.3f} \n".format(self.r_eff))
            strD = ("   R core/mantle = {:1.3f} \n".format(self.rcoremant))
            strE = (" Type: " + str(self.typ) + "\n")
            return (str0 + strA + strB + strC + strD + strE)
        if self.layered  is False:
            str0 = "Spherical particles\n"
            strA = ("nr =" + str(self.nr) + "))\n")
            strB = ("ni =" + str(self.ni) + "))\n")
            strC = ("   R_eff = {:1.3f} \n".format(self.r_eff))
            strD = (" Type: " + str(self.typ) + "\n")
            return (str0 + strA + strB + strC + strD)

    def load_coefs(self,filename_list, ncoefsMAX=4001, nmatMAX=4):
        """ Method to load files with expansion coeficients into the Aerosol
        object."""

        nwavels = len(filename_list)
        # Creating array to receive the coefficients
        supercoefin = np.zeros((nwavels,nmatMAX,nmatMAX,ncoefsMAX), order='F')
        superncoefin = np.zeros(nwavels, order='F')

        for i,filename in enumerate(filename_list):
            ncoef, coefs = readmie.file2coefs(filename)

            # Store the coefficients for each wvl
            supercoefin[i,:,:,:] = coefs
            superncoefin[i] = ncoef

        self.coefs = supercoefin
        self.ncoefs = superncoefin


# --------------
# FUNCTIONS
#--------------

# Small utilities
def calc_azimuth(phase, sza, emission, deg=True):
    """Compute the azimuth angle from the geometric
    data.

    Parameters
    ----------
    phase : float or array
        phase angle
    sza : float or array
        solar zenith angle
    emission : float or array
        emission angle
    deg : Boolean, optional
        If True, the output is given in degrees; in radians otherwise.
        By default True

    Returns
    -------
    azimuth : float or array
        the azimuthal angle
    """

    theta = np.radians(sza)
    thetap = np.radians(emission)
    alpha = np.radians(phase)
    t1 = np.cos(alpha) - (np.cos(theta)*np.cos(thetap))
    t2 = np.sin(theta)*np.sin(thetap)

    c_delta_phi = t1/t2
    c_delta_phi[t2<1e-6] = 1. # if denominator is too small, set cos(phi) to 1.
    c_delta_phi[c_delta_phi>1.] = 1.
    c_delta_phi[c_delta_phi<-1.] = -1.

    if deg is True:
        delta_phi = np.degrees(np.arccos(c_delta_phi))
    else:
        delta_phi = np.arccos(c_delta_phi)

    azimuth = delta_phi
    return azimuth


def get_cosbeta(PHA,SZA,EMI,AZI):
    """Calculate the rotation angle between the local meridian plane and the
    scattering plane.

    Parameters
    ----------
    PHA :
        phase angle (deg)
    SZA :
        solar zenith angle (deg)
    EMI :
        emission angle (deg)
    AZI :
        azimuthal angle (deg)

    Returns
    -------
    cb :
        cosine of angle beta (rad)

    """

    sgn = np.ones(len(AZI))
    #sgn[AZI<0.] = 1
    #sgn[AZI>0.] = -1
    #sgn = (-1.* AZI>=np.pi) + (1. * AZI<np.pi)
    #sgn = 1.
    num = np.cos(np.pi*SZA/180.)-np.cos(np.pi*EMI/180.)*np.cos(np.pi*PHA/180.)
    denom = (sgn*np.sin(np.pi*EMI/180.)*np.sin(np.pi*PHA/180.))
    cb = num/denom
    cb[denom==0] = 0.
    cb[cb>1.] = 1.
    cb[cb<-1.] = -1.

    return cb


def sunblackbody(L, Ts=5750):
    """ Compute the blackbody radiance of the Sun at given
    wavelength

    Parameters
    ----------
    L : float or array
        wavelength in metres
    Ts : float, optional
        Surface temperature of the star, in K.
        By default 5750 K

    Returns
    -------
    B : float or array
        Blackbody radiance in W/m2/sr/um

    """

    h = 6.63e-34  # Planck's constant
    c = 2.99e8  # Speed of light
    kb = 1.38e-23  # Boltzmann cst
    #Ts = 5750  # Sun's surf temp.

    A = (2*h*c*c) / (L**5)
    AA = 1. / (np.exp((h*c) / (L*kb*Ts)) - 1)
    B = A * AA  # units : W/m2/sr/m
    B = B * 1e-6  # W/m2/sr/um

    return B


# Main functions
def mie_code(aerosols, wavelengths, output=False, delta=1e-8, cutoff=1e-8, thmin=0, thmax=180,
             nsubr=50, ngaur=60, ncoefsMAX=4001,
             nfouMAX=4001, nmuMAX=201, nmatMAX=4):
    """ Compute Mie expansion coefficients for Aerosol object.
    Requires the module_mie module.

    Parameters
    ----------
    aerosols : Aerosol object
        an input aerosol type model containing all the modeling parameters
    wavelengths : array
        list of wavelengths for which computations should be performed
    delta: float, optional
        truncation of the Mie sum. Default 1e-8
    cutoff: float, optional
        cutoff value for the particle size distribution. Default 1e-8
    thmin: float, optional
        minimal value of phase angle (in degrees). Default 0
    thmax: float, optional
        maximal phase angle. Default 180.
    nsubr: integer, optional
        number of subintervals for the distribution. Default 50
    ngaur: integer, optional
        number of Gauss points used in the calculations. Default 60
    ncoefsMAX: int, optional
        max number of coefs for the Mie expansion. Default 4001.
        Warning: changing this might conflict with Fortan modules
    nmatMAX: int, optional
        number of Stokes elements to compute. Should be 1, 3 or 4.
        Default 4
    output : Bool, optional
        if True, expansion coefficients are written in a file with name in the form
        scfile_name = aerosols.specie + '.sc.' + '{:06.3f}'.format(wav)

    Returns
    -------
    Stores expansion coefficients in aerosols object

    """

    # ---------------------------
    # Mie calculations parameters
    # ---------------------------

    weight2 = 0.3

    nwavels = len(wavelengths)

    # Creating array to receive the coefficients
    supercoefin = np.zeros((nwavels,nmatMAX,nmatMAX,ncoefsMAX), order='F')
    superncoefin = np.zeros(nwavels, order='F')

    qexts = np.zeros(nwavels)
    sexts = np.zeros(nwavels)
    qscas = np.zeros(nwavels)
    sscas = np.zeros(nwavels)
    asyms = np.zeros(nwavels)

    #Getting aerosols parameters
    r_eff = aerosols.r_eff
    v_eff = aerosols.v_eff
    par3 = aerosols.par3
    specie = aerosols.typ
    idis = aerosols.psd  # index of the particle size dist

    # Getting extrema for radii
    rmin, rmax = mie.rminmax(idis, r_eff, v_eff, par3, weight2, cutoff)
    # -----
    # LOOP ON WAVELENGTHS
    # -----

    print('Beginning of Mie program')

    for i, wav in enumerate(wavelengths):
        print('Wavelength {:06.7f}'.format(wav))
        nr = aerosols.nr[i]
        ni = aerosols.ni[i]
        m = nr - 1j * ni

        scfile_name = specie + '.sc.' + '{:06.3f}'.format(wav)

        # calculation of the scattering matrix
        u, wg, F, miec, nangle = mie.scatmat(m, wav, idis, nsubr, ngaur, rmin,
                                             rmax, r_eff, v_eff, par3, weight2,
                                             delta)

        ncoefs = nangle
        #expansion of the matrix
        coefs = matrix_expansion(ncoefs, nangle, u, wg, F)

        # Store the coefficients for each wvl
        supercoefin[i,:,:,:] = coefs
        superncoefin[i] = ncoefs

        qexts[i] = miec[3] # get Qext somewhere
        sexts[i] = miec[1] # get Sext somewhere
        qscas[i] = miec[1] # get Qext somewhere
        sscas[i] = miec[0] # get Sext somewhere

        #-----------------------------------------------------------------------
        #        Calculate the asymmetry parameter:
        #-----------------------------------------------------------------------
        if nangle >= 1:
            cosbar = coefs[0, 0, 1] / 3.
        else:
            cosbar = 0.

        asyms[i] = cosbar

        #------------------------------------------------------------------
        #        Write the coefficients to the output file:
        #------------------------------------------------------------------
        if output is True:
            mie.writsc(scfile_name, idis, nsubr, ngaur, coefs, ncoefs, cosbar,
                        miec, wav, nr, ni, rmin, rmax, r_eff, v_eff, par3)

        # end of wvl loop


    # Storing the coefficients in the aerosols object
    aerosols.coefs = supercoefin
    aerosols.ncoefs = superncoefin
    aerosols.asym = asyms
    aerosols.sext = sexts
    aerosols.qext = qexts
    aerosols.ssca = sscas
    aerosols.qsca = qscas
    aerosols.ssalb = sscas/sexts

    print('End of Mie program')


def mie_shell(aerosols, wavelengths, output=False, delta=1e-8, cutoff=1e-8, thmin=0, thmax=180,
              nsubr=20, ngaur=20, nlaysMAX=50, ncoefsMAX=4001,
              nfouMAX=4001, nmuMAX=201, nmatMAX=4):
    """ Compute Layered spheres Mie expansion coefficients for Aerosol object.
    Requires the module_mieshell module.

    Parameters
    ----------
    aerosols : Aerosol object
        an input aerosol type model containing all the modeling parameters
    wavelengths : array
        list of wavelengths for which computations should be performed
    delta: float, optional
        truncation of the Mie sum. Default 1e-8
    cutoff: float, optional
        cutoff value for the particle size distribution. Default 1e-8
    thmin: float, optional
        minimal value of phase angle (in degrees). Default 0
    thmax: float, optional
        maximal phase angle. Default 180.
    nsubr: integer, optional
        number of subintervals for the distribution. Default 50
    ngaur: integer, optional
        number of Gauss points used in the calculations. Default 60
    ncoefsMAX: int, optional
        max number of coefs for the Mie expansion. Default 4001.
        Warning: changing this might conflict with Fortan modules
    nmatMAX: int, optional
        number of Stokes elements to compute. Should be 1, 3 or 4.
        Default 4
    output : Bool, optional
        if True, expansion coefficients are written in a file with name in the form
        scfile_name = aerosols.specie + '.sc.' + '{:06.3f}'.format(wav)

    Returns
    -------
    Stores expansion coefficients in aerosols object

    """

    # ---------------------------
    # Mie calculations parameters
    # ---------------------------
    par3 = 0.25  # this last parameter is only used for some PSD
    weight2 = 0.0

    nwavels = len(wavelengths)

    # Creating array to receive the coefficients
    supercoefin = np.zeros((nwavels,nmatMAX,nmatMAX,ncoefsMAX), order='F')
    superncoefin = np.zeros(nwavels, order='F')

    qexts = np.zeros(nwavels)
    sexts = np.zeros(nwavels)
    qscas = np.zeros(nwavels)
    sscas = np.zeros(nwavels)
    asyms = np.zeros(nwavels)

    #Getting aerosols parameters
    r_eff = aerosols.r_eff
    v_eff = aerosols.v_eff
    specie = aerosols.typ
    idis = aerosols.psd  # index of the particle size dist

    # Getting extrema for radii
    rmin, rmax = mie.rminmax(idis, r_eff, v_eff, par3, weight2,
                                cutoff)
    # -----
    # LOOP ON WAVELENGTHS
    # -----

    print('Beginning of Mie program')

    for i, wav in enumerate(wavelengths):
        print('Wavelength {:06.3f}'.format(wav))
        nr_mantle = aerosols.nr[i]
        ni_mantle = aerosols.ni[i]
        m1 = (nr_mantle + 1j * ni_mantle)

        nr_core = aerosols.nr_core[i]
        ni_core = aerosols.ni_core[i]
        m2 = (nr_core + 1j * ni_core)
        # Re + i*Im is necessary for BHCOAT

        ratio = aerosols.rcoremant

        scfile_name = specie + '.sc.' + '{:06.3f}'.format(wav)

        # calculation of the scattering matrix
        u, wg, F, miec, nangle = mieshell.scatmat(m1,m2, wav, idis, nsubr,
                                                  ngaur, rmin, rmax, r_eff,
                                                  v_eff, par3, ratio, weight2,
                                                  delta)

        ncoefs = nangle
        #expansion of the matrix
        coefs = matrix_expansion(ncoefs, nangle, u, wg, F)

        # Store the coefficients for each wvl
        supercoefin[i,:,:,:] = coefs
        superncoefin[i] = ncoefs

        qexts[i] = miec[3] # get Qext somewhere
        sexts[i] = miec[1] # get Sext somewhere
        qscas[i] = miec[2] # get Qsca somewhere
        sscas[i] = miec[0] # get Ssca somewhere

        #-----------------------------------------------------------------------
        #        Calculate the asymmetry parameter:
        #-----------------------------------------------------------------------
        if nangle >= 1:
            cosbar = coefs[0, 0, 1] / 3.
        else:
            cosbar = 0.

        asyms[i] = cosbar

        #------------------------------------------------------------------
        #        Write the coefficients to the output file:
        #------------------------------------------------------------------
        if output is True:
            mie.writsc(scfile_name, idis, nsubr, ngaur, coefs, ncoefs, cosbar,
                        miec, wav, nr_mantle, ni_mantle, rmin, rmax, r_eff, v_eff, par3)

        # end of wvl loop


    # Storing the coefficients in the aerosols object
    aerosols.coefs = supercoefin
    aerosols.ncoefs = superncoefin
    aerosols.asym = asyms
    aerosols.sext = sexts
    aerosols.qext = qexts
    aerosols.ssca = sscas
    aerosols.qsca = qscas
    aerosols.ssalb = sscas/sexts

    print('End of Mie program')
    #return u,F, miec


def matrix_expansion(ncoefs, nangle, u, wg, F):
    """ Expansion of a scattering matrix in fourier coefficients.

    Used only as an alias for mie.devel

    Parameters
    ----------
    ncoefs :
        the number of coefficients to be used in the expansion
    nangle:
        number of scattering angles
    u:
        cosine of scattering angles
    wg:
        gaussian weights associated with u
    F:
        scattering matrix

    Returns
    -------
    coefs : array (nmatMAX, nmatMAX, ncoefsMAX)
        the expansion coefficients

    """

    coefs = mie.devel(ncoefs, nangle, u, wg, F)
    return coefs


def read_mie_output(filename, full_output=False, nameout='stuff.dat'):
    """ Read Mie expansion coefficients from file

    Parameters
    ----------
    filename : string
        the name of the file containing the expansion coefficients
    full_output : Bool, optional
        if True, returns phase angles and full scattering matrix. If False,
        returns only phase and -Q/I
        Default is False
    nameout : string
        name of the output file

    Returns
    -------
    theta : array
        scattering angle
    Pl : array
        degree of linear polarization. If full_output is False
    F : array
        a (6,nangles) array with F11, F22, F33, F44, F12, F34 elements of
        the scattering matrix and degree of polarisation
        Only if full_output is True

    Also produces an output file with name nameout

    """

    theta, F = readmie.readmieoutput(filename, nameout)

    Pl = - F[4,:] / F[0,:]

    if full_output is True:
        return theta, F
    else:
        return theta, Pl


def dap_code(model, rename=False, output_name='modelA',
             path_output='./dap_database/',
             nlaysMAX=50, ncoefsMAX=4001, nfouMAX=4001,
             nmuMAX=201, nmatMAX=4, nmat=4, nmug=20):
    """ This function launches the DAP code to calculate the supermatrices
    produced by the doubling-adding code. Reads input from a model class.
    Requires the module module_dap.

    Parameters
    ----------
    model : Model object
        an input model containing all the modeling parameters
    rename: Bool
        if True, the user set output file is used for the resulting coefficient files
    output_name : string
        base of filename to be used for the Fourier output files
    path_output : string
        Path of folder where to store the output Fourier files. Folder is
        created if not existing.
    nlaysMAX : integer, optional
        Maximum number of layers. Default 50
    ncoefsMAX: int, optional
        max number of coefs for the Mie expansion. Default 4001.
        Warning: changing this might conflict with Fortan modules
    nfouMAX : integer, optional
        max number of Fourier coefficients. Default is 4001
    nmug : integer, optional
        number of Gauss points used in the calculations. Default 20
    nnuMAX: int, optional
        maximal number of Gauss points
    nmatMAX: int, optional
        max number of Stokes elements. Should be 1, 3 or 4.
        Default 4
    nmat: int, optional
        number of Stokes elements to compute. Should be 1, 3 or 4.
        Default 4

    Returns
    -------
        produces an output file containing Fourier coefficients for the given
        model

    """

    # Checking that the output directory exists
    # if not, creating it
    try:
        os.makedirs(os.path.normpath(path_output))
    except OSError:
        if not os.path.isdir(os.path.normpath(path_output)):
            raise

    #-----------------------------------------------------------------------
    #     Some parameter values:
    #
    #     nmat: size of Stokes parameters (1, 3, or 4)
    #     nmug: number of Gauss points
    #     asurf: surface albedo
    #     nlays: the number of atmospheric layers
    #     nmat: size of Stokes parameters (1, 3, or 4)
    #     nmug: number of Gauss points
    #     asurf: surface albedo
    #-----------------------------------------------------------------------
    nsupMAX = nmuMAX * nmatMAX

    # Reading basic properties of the model
    asurf = model.asurf
    surfmat = surface_check(model,nmug, nmat=nmat, nmuMAX=nmuMAX)
    nlays = len(vars(model.layers))  # number of atmospheric layers
    nwvl = len(model.wvl_list)

    ncoefs = np.zeros(nlaysMAX, order='F')
    wavels = model.wvl_list
    nwavels = len(wavels)

    # Creating array to receive the coefficients
    coefin = np.zeros((nmatMAX,nmatMAX,ncoefsMAX,nlaysMAX), order='F')
    coefs = np.zeros((nmatMAX,nmatMAX,ncoefsMAX,nlaysMAX), order='F')
    ncoefin = np.zeros((nlaysMAX), order='F')
    model.name = ['']*len(model.wvl_list)

    # some global parameters
    gravity = model.gravity
    dpol = model.dpol
    mma = model.mma

    print('Beginning of DAP program')

    # ---------------------
    # Loop on wavelengths
    # ---------------------
    for z, wav in enumerate(wavels):
        #get coefs for each wavelength
        taus = np.zeros(nlaysMAX, order='F')  # all values of tau at wvl z
        taus_g = np.zeros(nlaysMAX, order='F')  # all values of tau_g at wvl z
        basca = np.zeros(nlaysMAX, order='F')  # all values of basca
        baabs = np.zeros(nlaysMAX, order='F')  # all values of baabs
        ssalbs = np.zeros(nlaysMAX, order='F')  # all values of s.scat albedoes
        pres = np.zeros(nlaysMAX, order='F')  # pressure levels for each layer

        print('Wavelength {:06.7f} microns'.format(wav))

        # Loop on layers
        l = 0  # layer number
        for layer_name, layer in vars(model.layers).items():

            #  reading information contained in all layers
            # reading the coefficients of each layer for the current wavelength
            taus[l] = layer.tau[z]  #value of tau at wvl z in layer l
            taus_g[l] = layer.tau_g[z]  #value of tau_g at wvl z in layer l
            pres[l] = layer.press

            # if the layer is transparent, then ignore the aerosols
            # coefficients
            if max(layer.tau)!=0:
                coefin[:,:,:,l] = layer.mixed_aerosols.coefs[z,:,:,:]
                ncoefin[l] = layer.mixed_aerosols.ncoefs[z]
                ssalbs[l] = layer.mixed_aerosols.ssalb[z]
                # Calculate the aerosol scattering and absorption optical
                # thicknesses,
                basca[l] = ssalbs[l] * taus[l]
                baabs[l] = (1. - ssalbs[l]) * taus[l]

            print('{}.sc.{:06.7f}'.format(layer.mixed_aerosols.typ, wav))
            l = l + 1

        layorder = np.argsort(pres[:l]) # finding the order of pressures
        layorder = layorder[::-1]  # putting layers in order (highest pres below)

        taus[:l] = taus[layorder]  # putting the taus in right order
        taus_g[:l] = taus_g[layorder]  # putting the taus_g in right order
        pres[:l] = pres[layorder]  # putting the pressures in right order

        # More reordering
        coefin[:,:,:,:l] = coefin[:,:,:,layorder]
        ncoefin[:l] = ncoefin[layorder]
        ssalbs[:l] = ssalbs[layorder]
        basca[:l] = basca[layorder]
        baabs[:l] = baabs[layorder]

        # Calculate the molecular parameters of the atmosphere:
        ri = model.rindex_gas[z]
        bmsca, bmabs, coefsm = dap.bmolecules(wav, nlays, pres, dpol, ri, mma, gravity)

        model.coefsm = coefsm

        # Storing the effective scattering and absorption opacities
        for layer_name, layer in vars(model.layers).items():
            # identify position of the current layer
            lev = (pres==layer.press)

            # force user-define rayleigh opacity
            if layer.rayscat is False:
                bmsca[lev] = layer.tau_ray[z]

            layer.bmsca[z] = bmsca[lev]
            layer.bmabs[z] = bmabs[lev]
            layer.basca[z] = basca[lev]
            layer.baabs[z] = baabs[lev]

        #---------------------------------------------------------------------
        #     Open the Fourier coefficients output file:
        #---------------------------------------------------------------------
        outputname = 'fou_' + '{:4.3f}'.format(wav) + '.dat'

        #---------------------------------------------------------------
        #     Calculate the combined expansion coefficients
        #---------------------------------------------------------------
        for i in np.arange(nlays):
            ncoefs[i] = max(ncoefin[i], 2)
            # multiply all coefs by the associated optical thickness
            # in each layer
            com = bmsca[i,np.newaxis,np.newaxis] * coefsm
            coa = basca[i,np.newaxis,np.newaxis,np.newaxis] * coefin[:,:,:,i]
            # if the opacities are too small, nullify coefs
            if ((bmsca[i]+basca[i]) < 1e-10):
                coefs[:,:,:,i]= 0.
            else:
                # otherwise, scale coefs according relative opacities
                coefs[:,:,:,i]= (com+coa)/(bmsca[i]+basca[i])

        model.coefst = coefs

        #-----------------------------------------------------------------------
        #     Calculate the total optical thicknesses and albedos:
        #-----------------------------------------------------------------------
        bmabs = taus_g
        b = bmsca + basca + baabs + bmabs
        model.bmsca = bmsca
        model.basca = basca
        model.bmabs = bmabs
        model.baabs = baabs
        model.b = b
        a = np.zeros(len(b))
        # If b is zero, then a should also be 0
        #a[b==0] = 0.0
        a[b!=0] = (bmsca[b!=0]+basca[b!=0])/b[b!=0]
        model.a = a

        #-----------------------------------------------------------------------
        #     Call the doubling-adding routine:
        #-----------------------------------------------------------------------
        dap.adding(outputname,a,b,coefs,ncoefs,nlays,nmug,nmat,surfmat)

        # Naming the model with check for Windows paths
        print('fou_{:4.7f}.dat'.format(wav))
        if rename is True:
            output_file = path_output + output_name + '_{:4.7f}.dat'.format(wav)
            output_file = os.path.normpath(output_file)
            model.name[z] = output_file
            os.rename('fou_{:4.3f}.dat'.format(wav),output_file)
        else:
            output_file = path_output + 'fou_{:4.7f}.dat'.format(wav)
            output_file = os.path.normpath(output_file)
            model.name[z] = output_file
            os.rename('fou_{:4.3f}.dat'.format(wav),output_file)
        print('End of DAP program')


def read_dap_output(phase, sza, emission, filename, beta=None, phi=None,
                    ngeosMAX=100000, nmuMAX=300, nfouMAX=2000, nmatMAX=4):
    """ Reads the supermatrices coefficients for given geometry

    Parameters
    ----------
    phase : float or array
        phase angle (deg)
    sza : float or array
        solar zenith angle (deg)
    emission : float or array
        emission angle (deg)
    filename : string
        name of the Fourier file to be read
    beta : None, float or array, optional
        angle between the meridian plane and the scattering plane (deg)
    phi : None, float or array, optional
        azimuthal angle (deg)
    nfouMAX : integer, optional
        max number of Fourier coefficients. Default is 4001
    ngeosMAX : integer, optional
        max number of geometries. Default 100000
    nnuMAX: int, optional
        maximal number of Gauss points
    nmatMAX: int, optional
        max number of Stokes elements. Should be 1, 3 or 4.
        Default 4


    Returns
    -------
    I : array (same shape as phase)
        Stokes element I, normalised with input flux unity (not Pi)
    Q : array (same shape as phase)
        Stokes element Q, normalised with input flux unity (not Pi)
    U : array (same shape as phase)
        Stokes element U, normalised with input flux unity (not Pi)
    V : array (same shape as phase)
        Stokes element V, normalised with input flux unity (not Pi)

    All elements are given assuming normal reflection (i.e. multiply by
    cos(theta0) if you want real observed flux)

    """

    ngeos = len(phase)
    betaF = np.zeros(ngeosMAX, order='F')
    azimuthF = np.zeros(ngeosMAX, order='F')

    if phi is None:
        # Getting needed geometry
        azimuth = calc_azimuth(phase, sza, emission, deg=False)  # azimuth angle in rads
        azimuth = np.pi - azimuth  # corr. due to diff definitions
        #azimuth = azimuth  # corr. due to diff definitions
        azimuthF[:ngeos] = np.degrees(azimuth)
    else:
        azimuth = phi[:ngeos]
        azimuthF[:ngeos] = phi[:ngeos]


    if beta is None:
        azimuth = azimuth[:ngeos]
        beta = get_cosbeta(phase, sza, emission, azimuth)  # rotation angle beta
        beta = np.arccos(beta) #warning: still in radians here
        betaF[:ngeos] = np.degrees(beta)
    else:
        betaF[:ngeos] = beta[:ngeos]


    ngeos = len(phase)

    #Preparing vectors for FORTRAN function
    szaF = np.zeros(ngeosMAX, order='F')
    emissionF = np.zeros(ngeosMAX, order='F')

    szaF[:ngeos] = sza
    emissionF[:ngeos] = emission
    # make sure all input angles are in degrees

    # Reading Stoke vector
    rfou = np.zeros((nmatMAX*nmuMAX,nmuMAX,nfouMAX+1), order='F')
    Sv = geos.read_dap(filename, ngeos, szaF, emissionF, azimuthF, betaF, rfou)
    del(rfou)

    # storing output in proper Stokes elements
    I = Sv[0,:ngeos]
    Q = Sv[1,:ngeos]
    U = Sv[2,:ngeos]
    V = Sv[3,:ngeos]

    # scaling unit input flux
    I = I/np.pi
    Q = Q/np.pi
    U = U/np.pi
    V = V/np.pi

    return I,Q,U,V


def compute_model(atm_model, force=False,
               path_input='./dap_database/', set_taus=False, rename=False,
               output_name='modelA', nmug_mie=20, nmug=20, nsubr=50, nmat=4):
    """
    Compute the Fourier files associated with a Model object.

    Runs mie_code for all Aerosol objects and dap_code on the model

    Parameters
    ----------
    atm_model : Model object
        an input model containing all the modeling parameters
    force : bool, optional
        if True, existing Fourier files are overwritten
        if False, Fourier files are computed only if atm_model has no
        associated file yet.
    path_output : string
        Path of folder where to store the output Fourier files. Folder is
        created if not existing.
    set_taus : bool
        if True, the opacities in atm_model are computed using the column
        densities, instead of the user-defined tau
    rename: Bool
        if True, the user set output file is used for the resulting coefficient files
    output_name : string
        base of filename to be used for the Fourier output files
    nmug : integer, optional
        number of Gauss points used in the DAP calculations. Default 20
    nmug_mie : integer, optional
        number of Gauss points used in the Mie calculations. Default 20
    nsubr : int, optional
        Number of subintervals for Mie scattering.
        Default 50
    nmat: int, optional
        number of Stokes elements to compute. Should be 1, 3 or 4.
        Default 4

    Returns
    -------
    Computes the Fourier files related to the given model. Also stores
    their names in the model object.

    """

    # Get wvl list
    wvl = atm_model.wvl_list

    # If the model is not yet computed or is forced to
    if atm_model.name[0] == '' or force is True:
        # Execute Mie on all aerosols types on all layers
        for lay, layer in vars(atm_model.layers).items():
            print('In layer '+lay+':')
            #If there is already an aerosol mix, we overwrite it
            if hasattr(layer,'mixed_aerosols') is True:
                del(layer.mixed_aerosols)
            for aero_name, aero in vars(layer).items():
                if isinstance(aero, Aerosols):
                    if aero.layered is False:
                        mie_code(aero, atm_model.wvl_list, ngaur=nmug_mie, nsubr=nsubr)
                    else:
                        mie_shell(aero, atm_model.wvl_list, ngaur=nmug_mie, nsubr=nsubr)

            layer.mix_aerosols() #mix aerosols

        # Set the opacities
        if set_taus is True:
            for lay, layer in vars(atm_model.layers).items():
                layer.tau = layer.col_dens * layer.mixed_aerosols.sext

        #execute DAP
        # making sure that the directory we write to is the same we'll read
        # from
        dap_code(atm_model, rename=rename, output_name=output_name, nmug=nmug,
                 nmat=nmat, path_output=path_input)


def read_model(atm_model,data,step=20, force=False,
               path_input='./dap_database/', set_taus=False, rename=False,
               output_name='modelA', nmug_mie=20, nmug=20, nsubr=50, nmat=4):
    """ to read the files associated with a model.
    INPUTS:
        atm_model : a Model object with all the input parameters
        data : a Data object with the observations
    KEYWORDS:
        step : step btw two points used for the fits
        force : if False, existing Fourier files are not overwritten; if True
            existing files are replaced by newer versions
        path_input : path of the fourier DAP files
        set_taus: if True, will set opacities following scattering cross section and column density
        rename: if true, output_name is used
        output_name: custom name radical for the output files of the DAP code
        nmug: number of Gauss points for Mie and DAP calculations
        nmat: number of Stokes elements to compute
    OUTPUT: returns the reflected Stokes vectors for the exact geometry of the
    observation given by data and for the model considered
    """

    # Get wvl list
    wvl = atm_model.wvl_list

    #Get geom
    atm_model.geom.phase = data.phase[::step]
    atm_model.phase = data.phase[::step]
    atm_model.geom.sza = data.geo.sza[::step]
    atm_model.geom.emission = data.geo.emission[::step]
    atm_model.geom.calc_azimuth()
    atm_model.geom.calc_beta()

    # Create table to store output
    n_pts = len(data.phase[::step])
    It = np.zeros((len(wvl),n_pts))
    Qt = np.zeros((len(wvl),n_pts))
    Ut = np.zeros((len(wvl),n_pts))
    Vt = np.zeros((len(wvl),n_pts))

    # compute Fourier file
    compute_model(atm_model, force=force, path_input=path_input,
                  set_taus=set_taus, rename=rename, output_name=output_name,
                  nmug_mie=nmug_mie, nmug=nmug, nsubr=nsubr, nmat=nmat)

    #read files and store result
    for j,w in enumerate(wvl):
        print('Reading {}'.format(atm_model.name[j]))
        I,Q,U,V = read_dap_output(data.phase[::step],data.geo.sza[::step],data.geo.emission[::step],atm_model.name[j],beta=atm_model.geom.beta, phi=atm_model.geom.azimuth)
        It[j,:] = I*np.cos(np.radians(atm_model.geom.sza))
        Qt[j,:] = Q*np.cos(np.radians(atm_model.geom.sza))
        Ut[j,:] = U*np.cos(np.radians(atm_model.geom.sza))
        Vt[j,:] = V*np.cos(np.radians(atm_model.geom.sza))
    atm_model.I = It

    # Compute adjusted radiance
    B = sunblackbody(np.array(atm_model.wvl_list)*1e-6, Ts=atm_model.Ts)
    omegas = np.pi * (atm_model.Rs/atm_model.Dps)**2
    atm_model.I2 = atm_model.I * B[:,np.newaxis] * omegas

    atm_model.Q = Qt
    atm_model.U = Ut
    atm_model.V = Vt
    atm_model.P = -Qt/It


def gen_clouds():
    model = Model()
    model.wvl_list = [1.101]
    model.layers.haze.aerosols.r_eff = 0.25
    model.layers.haze.col_dens = 0.2
    model.layers.haze.aerosols.typ = 'H'
    model.layers.gastop.tau[:] = 0.0
    model.layers.gastop.col_dens = 0.0
    model.layers.gasbelow.tau[:] = 0.0
    model.layers.gasbelow.col_dens = 0.0
    return model


def rotate_stokes(Q,U,beta):
    """ Compute values of Q and U after rotation from one reference
	plane to another plane separated by an angle beta

    Parameters
    ----------
    Q : float or array
        Stokes Q
    U : float or array
        Stokes U
    beta : float or array
        Angle between two reference planes (radians)

    Returns
    -------
    newQ : float or array
        rotated Q
    newU : float or array
        rotated U

    """

    newQ = np.cos(2*beta)*Q + np.sin(2*beta)*U
    newU = -np.sin(2*beta)*Q + np.cos(2*beta)*U

    return newQ, newU


def binned_average(x,y,xbins, errmean=True, weighted=True, sigmas=1.):
    """ This function computes binned averages
    Input:
        x,y : data to be binned
        xbins : bins in which to average
        errmean : if True, the std returned is the error of the mean
        and not the dispersion
    Output :
        mean and std
    """

    # values of averaged bins
    xmid = xbins[:-1]+0.5*np.diff(xbins)

    # Inverse of sigmas squared
    sigm2 = 1/sigmas**2

    if weighted is True:
        # this is \sum 1/sigma_i^2
        n = np.histogram(x,bins=xbins,weights=sigm2)[0]
        # this is \sum x_i / sigma_i^2
        sy = np.histogram(x,bins=xbins,weights=y*sigm2)[0]
        moy = sy/n
        # this is T^2 = \sum (x_i-moy)**2/ sigma_i^2
        #sigma_intermed = np.histogram(x,bins=xbins,weights=sigm2*(y-moy)**2)[0]
        #sigma : sqrt(T**2/n)
        #sigma = np.sqrt(sigma_intermed/n)
        sigma = np.sqrt(1./n)
    else:
        n = np.histogram(x,bins=xbins)[0]
        sy = np.histogram(x,bins=xbins,weights=y)[0]
        sy2 = np.histogram(x,bins=xbins,weights=y*y)[0]
        moy = sy/n
        sigma = np.sqrt(sy2/n - moy**2)
        if errmean is True:
            sigma = sigma/np.sqrt(n)

    return xmid,moy,sigma


def planet_pixels(models, alpha=[10], npix=15, force=False, set_taus=False, rename=True,
                  output_names=['modelA','modelB'], fixed_pattern=False,
                  input_pattern=None, cusp=False, thresh_lat=50., patchy=True,
                  xscale=0.1, yscale=0.01,
                  bands=False, bands_lats=[-90,90],
                  fclouds=[0.5,0.5], constant_fcloud=False, sscloud=False,
                  sigma_c=10., delta_c=[0.], nmug_mie=20, nmug=20, nsubr=50,
                  nmat=4, pixscaler=1, adaptive_pixels=False):
    """ Generate disk-resolved images of a planet according to model

    Parameters
    ----------
    models : list of Model objects
        models to use in computations
    alpha : array
        phase angles for which to compute
    npix : int
        number of pixels (total number of pixels will be npix**2)
    force : bool, optional
        if True, will force recalculation of model
    set_taus : bool, optional
        if True, will set opacities following scattering cross section and column density
    rename : bool, optional
        if True, model output files will be renamed
    output_names : list of strings
        list of names of the models, used for the name of the Fourier files
    fixed_pattern : bool, optional
        if True, a cloud pattern is generated at start and then
        reused for all phase angle after.
    input_pattern: array, optional
        an existing pattern that can be used as a starter
        (caution: must have size nphase*npix*npix)
    cusp : bool, optional
        if True, polar cusps are created
    thresh_lat : float, optional
        defines the latitude above which the cusps exist
    patchy : bool, optional
        if True, patchy clouds are generated
    fcloud : array, optional
        fraction of the planet to be covered with clouds
        should have same length as models input list
    constant_fcloud : bool, optional
        if True, the factor fcloud applies not to the whole
        planet but to the lit part of the planet
    sscloud : bool, optional
        if True, a subsolar cloud is created
    sigma_c : float, optional
        extend in degrees of the subsolar cloud with respect to the SSP. Cloud
        exists for SZA<sigma_c
    delta_c : float, optional
        offset in degrees the position of the subsolar cloud with
        respect to subsolar point.
    bands : bool, optional
        if True, defines latitudinal bands
    bands_lats : array, optional
        array listing the borders of the bands.
        Ex: [-90, 45, 90] defines two bands
    nmug : int, optional
        number of Gauss point for DAP code
    nmug_mie : int, optional
        number of Gauss point for Mie code
    nmat : int, optional
        number of Stokes elements to compute
    nsubr : int, optional
        number of divisions for size dist in Mie calculations
    adaptive_pixels : bool, optional
        if True, npix increases with increasing phase angle (in sin**2 of
        alpha/2)
    pixscaler : float, optional
        factor used in combination with adaptive_pixels to set the
        rate of increase in pix number. Default 1
    xscale : float, optional
        for patchy clouds gives the typical size on x-axis, as a function of npix
    yscale : float, optional
        for patchy clouds gives the typical size on y-axis, as a function of npix

    Returns
    -------
    I,Q,U,V : arrays
        Stokes elements. I(alpha=0) being the geometric albedo
    P : array
        P is -Q/I
    Pqmin,Pqmax : arrays
        min and max values of -Q/I
    Plmin,Plmax : arrays
        min and max values of Pl
    Ptmin,Plmax : arrays
        min and max values of total polarization
    Imin,Imax : arrays
        min and max values of intensity

    Those parameters being stored in the first model object given as input.

    """

    # If onyl one model is given, assumes a full cover.
    if len(models)==1:
        full_disk=True
        patchy=False
    else:
        full_disk=False

    atm_model = models[0]
    wvl = atm_model.wvl_list
    nwvl = len(atm_model.wvl_list)
    nalpha = len(alpha)
    mpl.ioff()

    # Computing the model atmosphere
    # ------------------------------

    for M, model in enumerate(models):
        compute_model(model, force=force,
                      set_taus=set_taus, rename=rename, output_name=output_names[M],
                      nmug_mie=nmug_mie, nmug=nmug, nsubr=nsubr, nmat=nmat)

    #At start, no specific cloud cover
    picture_full = None
    atm_model.fcloud = np.zeros(len(alpha))
    atm_model.asym = np.zeros(len(alpha))

    # Preparing arrays
    # ------------------
    If = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    Qf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    Uf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    Vf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))

    phaf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    szaf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    emif = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    azif = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    betf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    latf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    lonf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    xf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))
    yf = np.nan*np.zeros((nwvl,nalpha,(2*npix)**2))

    if len(alpha)!=len(delta_c):
        delta_c = delta_c[0]*np.ones(len(alpha))

    # Loop on wvl
    # -------------
    for j,w in enumerate(wvl):

        # Loop on phase angle
        # -------------------
        for A,alph in enumerate(alpha):

            if fixed_pattern is False:
                picture_full=None

            if input_pattern is not None:
                picture_full=input_pattern[A,:,:]

            if adaptive_pixels is True:
                npix2 = np.ceil(npix * (1 + np.sin(np.radians(alph)/2.)**2))
                npix2 = int(npix2)
                print('npix=',npix2)
            else:
                npix2 = npix

            #Get geom
            ngeos, apix, theta0, theta, phi, beta, lats, longs, xs, ys = geos.getgeos(alph, npix2)

            theta0 = theta0[:ngeos]
            theta = theta[:ngeos]
            phi = phi[:ngeos]
            beta = beta[:ngeos]
            lats = lats[:ngeos]
            longs = longs[:ngeos]
            phase = np.ones(ngeos)*alph
            x = xs[:ngeos]
            y = ys[:ngeos]

            phaf[j,A,:ngeos] = alph*np.ones(ngeos)
            szaf[j,A,:ngeos] = theta0[:ngeos]
            emif[j,A,:ngeos] = theta[:ngeos]
            azif[j,A,:ngeos] = phi[:ngeos]
            betf[j,A,:ngeos] = beta[:ngeos]
            latf[j,A,:ngeos] = lats[:ngeos]
            lonf[j,A,:ngeos] = longs[:ngeos]
            xf[j,A,:ngeos] = xs[:ngeos]
            yf[j,A,:ngeos] = ys[:ngeos]


            # Create table to store output
            It = np.zeros((len(wvl),ngeos))
            Qt = np.zeros((len(wvl),ngeos))
            Ut = np.zeros((len(wvl),ngeos))
            Vt = np.zeros((len(wvl),ngeos))

            # call maskd_planet
            if npix!=npix2:
                picture_full = None

            picture, mask, picture_full, ncloud, asym = mask_planet(alpha=alph, npix=npix2,
                                                                    fixed_cover=picture_full,
                                                                    cusp=cusp,
                                                                    thresh_lat=thresh_lat,
                                                                    bands=bands,
                                                                    bands_lats=bands_lats,
                                                                    patchy=patchy,
                                                                    xscale=xscale,
                                                                    yscale=yscale,
                                                                    fclouds=fclouds,
                                                                    constant_fcloud=constant_fcloud,
                                                                    full_disk=full_disk,
                                                                    sscloud=sscloud,
                                                                    sigma_c=sigma_c,
                                                                    delta_c=delta_c[A])

            for pixtype,model in enumerate(models):
                print('Reading {}'.format(model.name[j]))
                phaseB = phase[mask==pixtype]
                theta0B = theta0[mask==pixtype]
                thetaB = theta[mask==pixtype]
                phiB = phi[mask==pixtype]
                betaB = beta[mask==pixtype]

                IB,QB,UB,VB = read_dap_output(phaseB,theta0B,thetaB,model.name[j],phi=phiB, beta=betaB)

                model.picture = np.copy(picture_full)

                It[j,mask==pixtype] = IB*np.cos(np.radians(theta0B))
                Qt[j,mask==pixtype] = QB*np.cos(np.radians(theta0B))
                Ut[j,mask==pixtype] = UB*np.cos(np.radians(theta0B))
                Vt[j,mask==pixtype] = VB*np.cos(np.radians(theta0B))

            If[j,A,:ngeos] = It[j,:]
            Qf[j,A,:ngeos] = Qt[j,:]
            Uf[j,A,:ngeos] = Ut[j,:]
            Vf[j,A,:ngeos] = Vt[j,:]

        atm_model.geom.phase = phaf
        atm_model.phase = phaf
        atm_model.geom.sza = szaf
        atm_model.geom.emission = emif
        atm_model.geom.azimuth = azif
        atm_model.geom.beta = betf
        atm_model.geom.latitude = latf
        atm_model.geom.longitude = lonf
        atm_model.geom.x = xf
        atm_model.geom.y = yf
        atm_model.npix = npix

        atm_model.I = If

        # Compute adjusted radiance
        B = sunblackbody(np.array(atm_model.wvl_list)*1e-6, Ts=atm_model.Ts)
        Rs = 696342000.
        Dvs = 108208930000.0
        omegas = np.pi * (atm_model.Rs/atm_model.Dps)**2
        atm_model.I2 = atm_model.I * B[:,np.newaxis,np.newaxis] * omegas

        atm_model.Q = Qf
        atm_model.U = Uf
        atm_model.V = Vf
        atm_model.P = -Qf/If
        atm_model.Pt = np.sqrt(Qf**2 + Uf**2 + Vf**2)/If
        atm_model.Pl = np.sqrt(Qf**2 + Uf**2)/If
        atm_model.Pv = Vf/If

        # PLOT
        font_size=14
        # BUG PIXEL SIZE?
        #pixsize = max(np.diff(lats))

        figsize = 850
        dpi = 90

    mpl.ion()


def plot_pixels(model, wvl_idx=0, display='grid', stokes='Ps', phase_idx=0,
                title='Polarization', cmap='YlOrRd',vmin=None,vmax=None, data_scale=1.,
                font_size=12, figsize=8, dpi=100):
    """ Function to nicely plot a resolved planet based on Model object

    Parameters
    ----------
    model : Model object
        a model object already computed and read with pmd.planet_pixels
    wvl_idx : int
        index of the wvl to be plotted
    phase_idx : int, optional
        index in the phase angle array to be plotted. Default is 0
    display : string, optional
        if 'grid', displays the planet as an orthographic projection of a
        sphere at a given phase angle.
        If 'map' displays results as function of latitude/longitude
        Default is 'grid'
    stokes : string, optional
        which Stokes element to plot. Allowed are 'Ps' (-Q/I), 'I', 'Q', 'U',
        'V', 'Pt' (total polarization), 'Pl' total linear pol, 'Pv' for V/I.
        Default is 'Ps'
    title : string, optional
        title of the figure
        Default is 'Polarization'
    cmap : string, optional
        a matplotlib colormap name, default is 'YlOrRd'
    vmin, vmax: floats or None, optional
        minimum and maximum range of values to plot, default are None
    data_scale :
        multiplier for the displayed quantity. plotted output is data_scale*data
        For example if you want P_l in percents, use data_scale=100.
    font_size : int, optional
        size of the font for the figure, default is 12
    figsize : float, optional
        size of the figure in inches
    dpi : int, optional
        dots per inch, resolution of the figure

    Returns
    -------
    Returns a figure with axes

    """

    npix=model.npix

    fig = mpl.figure(figsize=(figsize,figsize), dpi=dpi)
    ax = fig.add_subplot(111, aspect=1)


    if display == 'grid':
        X = model.geom.x[wvl_idx,phase_idx,:]
        Y = model.geom.y[wvl_idx,phase_idx,:]
        circ = mpl.Circle((0,0),1,color='gray')
        ax.add_patch(circ)
        ax.set_xlim(-1,1)
        ax.set_ylim(-1,1)
        bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        axh = bbox.height
        axw = bbox.width
        scalingx = np.ones_like(X)
        scalingy = np.ones_like(Y)
        area = (axh*dpi*axw*dpi)/(1.5*npix)**2
    if display == 'map':
        X = model.geom.longitude[wvl_idx,phase_idx,:]
        Y = model.geom.latitude[wvl_idx,phase_idx,:]
        ax.set_xlim(-90,90)
        ax.set_ylim(-90,90)
        bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        axh = bbox.height
        axw = bbox.width
        scalingx = 1./np.cos(np.radians(X))
        scalingy = 1./np.cos(np.radians(Y))
        area = (axh*dpi*axw*dpi)/(2*1.5*npix)**2

    if stokes=='I':
        Z = model.I[wvl_idx,phase_idx,:]
    if stokes=='Q':
        Z = model.Q[wvl_idx,phase_idx,:]
    if stokes=='U':
        Z = model.U[wvl_idx,phase_idx,:]
    if stokes=='V':
        Z = model.V[wvl_idx,phase_idx,:]
    if stokes=='Ps':
        Z = model.P[wvl_idx,phase_idx,:]
    if stokes=='Pl':
        Z = model.Pl[wvl_idx,phase_idx,:]
    if stokes=='Pt':
        Z = model.Pt[wvl_idx,phase_idx,:]
    if stokes=='Pv':
        Z = model.Pv[wvl_idx,phase_idx,:]

    Z = data_scale * Z

    ax.set_title(title)
    sc = ax.scatter(X, Y, c=Z,lw=0, marker='s',
                    s=area*scalingx*scalingy,
                    cmap=cmap, zorder=10,
                    vmin=vmin, vmax=vmax)
    fig.tight_layout(pad=1.2)
    cb = fig.colorbar(sc,pad=0.02, extend='both')
    cb.set_label(stokes,size=font_size)
    ax.set_aspect('equal')

    return fig,ax


def planet_integrated(models, alpha=[10], npix=15, force=False, set_taus=False,
                      rename=True, output_names=['modelA','modelB'], fixed_pattern=False,
                      input_pattern=None, cusp=False, thresh_lat=50., full_disk=False,
                      patchy=True, fclouds=[0.5,0.5], constant_fcloud=False,
                      xscale=0.1, yscale=0.01,
                      bands=False, bands_lats=[-90,90],
                      sscloud=False, sigma_c=10., delta_c=[0.], nmug_mie=20,
                      niter=1, nmug=20, nsubr=50, nmat=4,
                      adaptive_pixels=False):

    """ Function to generate disk-integrated images of a planet according to model

    Parameters
    ----------
    models : list of Model objects
        models to use in computations
    alpha : array
        phase angles for which to compute
    npix : int
        number of pixels (total number of pixels will be npix**2)
    force : bool
        if True, will force recalculation of model
    set_taus : bool
        if True, will set opacities following scattering cross section and column density
    rename : bool
        if True, model output files will be renamed
    output_names : list of strings
        list of names of the models, used for the name of the Fourier files
    fixed_pattern : bool
        if True, a cloud pattern is generated at start and then
        reused for all phase angle after.
    input_pattern: array
        an existing pattern that can be used as a starter
        (caution: must have size nphase*npix*npix)
    cusp : bool
        if True, polar cusps are created
    thresh_lat : float
        defines the latitude above which the cusps exist
    patchy : bool
        if True, patchy clouds are generated
    fcloud : array
        fraction of the planet to be covered with clouds
        should have same length as models input list
    constant_fcloud : bool
        if True, the factor fcloud applies not to the whole
        planet but to the lit part of the planet
    sscloud : bool
        if True, a subsolar cloud is created
    sigma_c : float
        extend in degrees of the subsolar cloud with respect to the SSP. Cloud
        exists for SZA<sigma_c
    delta_c : float
        offset in degrees the position of the subsolar cloud with
        respect to subsolar point.
    bands : bool
        if True, defines latitudinal bands
    bands_lats : array
        array listing the borders of the bands.
        Ex: [-90, 45, 90] defines two bands
    nmug : int
        number of Gauss point for DAP code
    nmug_mie : int
        number of Gauss point for Mie code
    nmat : int
        number of Stokes elements to compute
    nsubr : int
        number of divisions for size dist in Mie calculations
    adaptive_pixels : bool
        if True, npix increases with increasing phase angle (in sin**2 of
        alpha/2)
    pixscaler : float, optional
        factor used in combination with adaptive_pixels to set the
        rate of increase in pix number. Default 1
    xscale : float
        for patchy clouds gives the typical size on x-axis, as a function of npix
    yscale : float
        for patchy clouds gives the typical size on y-axis, as a function of npix

    Returns
    -------
    I,Q,U,V : arrays
        Stokes elements. I(alpha=0) being the geometric albedo
    P : array
        P is -Q/I
    Pqmin,Pqmax : arrays
        min and max values of -Q/I
    Plmin,Plmax : arrays
        min and max values of Pl
    Ptmin,Plmax : arrays
        min and max values of total polarization
    Imin,Imax : arrays
        min and max values of intensity

    Those parameters being stored in the first model object given as input.

    """

    # If onyl one model is given, assumes a full cover.
    if len(models)==1:
        full_disk=True
        patchy=False

    # Reading some parameters
    atm_model = models[0]
    wvl = atm_model.wvl_list
    nalpha = len(alpha)
    phases = np.zeros(nalpha)
    nwvl = len(wvl)
    ntypes = len(models)

    Nsteps = nalpha * niter * ntypes

    # Create table to store final output
    It = np.zeros((len(wvl),nalpha))
    Qt = np.zeros((len(wvl),nalpha))
    Ut = np.zeros((len(wvl),nalpha))
    Vt = np.zeros((len(wvl),nalpha))

    # Create tables to store raw variations
    Iall = np.ones((len(wvl),nalpha,niter))
    Qall = np.ones((len(wvl),nalpha,niter))
    Uall = np.ones((len(wvl),nalpha,niter))
    Vall = np.ones((len(wvl),nalpha,niter))

    # loop on models to compute
    for M, model in enumerate(models):
        compute_model(model, force=force,
                      set_taus=set_taus, rename=rename, output_name=output_names[M],
                      nmug_mie=nmug_mie, nmug=nmug, nsubr=nsubr, nmat=nmat)


    #At start, no specific cloud cover
    picture_full = None
    vecfcloud = np.zeros((len(alpha),len(fclouds)))
    vecasym = np.zeros((nalpha,niter))

    if len(alpha)!=len(delta_c):
        delta_c = delta_c[0]*np.ones(len(alpha))

    # ===================
    # Loop on phase angle
    # ===================

    for a,alph in enumerate(alpha):

        if fixed_pattern is False:
            picture_full=None

        if input_pattern is not None:
            picture_full=input_pattern[a,:,:]

        #Get geom of pixels
        if adaptive_pixels is True:
            npix2 = np.ceil(npix * (1 + np.sin(np.radians(alph)/2.)**2))
            npix2 = int(npix2)
            print('npix=',npix2)
        else:
            npix2 = npix

        ngeos, apix, theta0, theta, phi, beta, lats, longs, xs, ys = geos.getgeos(alph, npix2)
        phases[a] = alph

        theta0 = theta0[:ngeos]
        theta = theta[:ngeos]
        phi = phi[:ngeos]
        beta = beta[:ngeos]
        lats = lats[:ngeos]
        longs = longs[:ngeos]
        phase = np.ones(ngeos)*alph
        x = xs[:ngeos]
        y = ys[:ngeos]
        atm_model.geom.phase = phase
        atm_model.phase = phase
        atm_model.geom.sza = theta0
        atm_model.geom.emission = theta
        atm_model.geom.azimuth = phi
        atm_model.geom.beta = beta
        atm_model.geom.latitude = lats
        atm_model.geom.longitude = longs

        # store pixels values for each type of pixel
        Is = np.zeros((len(models),len(wvl),ngeos))
        Qs = np.zeros((len(models),len(wvl),ngeos))
        Us = np.zeros((len(models),len(wvl),ngeos))
        Vs = np.zeros((len(models),len(wvl),ngeos))

        # First mask
        picture, mask, picture_full, ncloud, asym = mask_planet(alpha=alph, npix=npix2,
                                                                fixed_cover=picture_full,
                                                                cusp=cusp,
                                                                thresh_lat=thresh_lat,
                                                                bands=bands,
                                                                bands_lats=bands_lats,
                                                                patchy=patchy,
                                                                xscale=xscale,
                                                                yscale=yscale,
                                                                fclouds=fclouds,
                                                                constant_fcloud=constant_fcloud,
                                                                full_disk=full_disk,
                                                                sscloud=sscloud,
                                                                sigma_c=sigma_c,
                                                                delta_c=delta_c[a])



        for pixtype,model in enumerate(models): #for each pixel type
            for j,w in enumerate(wvl): # and each wvl

                # if only one pattern read only relevant pixels
                if niter==1:
                    if pixtype in np.unique(mask):
                        phaseB = phase[mask==pixtype]
                        theta0B = theta0[mask==pixtype]
                        thetaB = theta[mask==pixtype]
                        phiB = phi[mask==pixtype]
                        betaB = beta[mask==pixtype]
                        print('Reading {}'.format(model.name[j]))
                        I,Q,U,V = read_dap_output(phaseB,theta0B,thetaB,model.name[j],phi=phiB, beta=betaB)
                        Is[pixtype,j,mask==pixtype] = I*np.cos(np.radians(theta0B))
                        Qs[pixtype,j,mask==pixtype] = Q*np.cos(np.radians(theta0B))
                        Us[pixtype,j,mask==pixtype] = U*np.cos(np.radians(theta0B))
                        Vs[pixtype,j,mask==pixtype] = V*np.cos(np.radians(theta0B)) # store current pixel type output
                    else:
                        I,Q,U,V = (0,0,0,0)
                else:
                    # if multiple patterns read all pixels
                    print('Reading {}'.format(model.name[j]))
                    I,Q,U,V = read_dap_output(phase,theta0,theta,model.name[j],phi=phi, beta=beta)
                    Is[pixtype,j,:] = I*np.cos(np.radians(theta0))
                    Qs[pixtype,j,:] = Q*np.cos(np.radians(theta0))
                    Us[pixtype,j,:] = U*np.cos(np.radians(theta0))
                    Vs[pixtype,j,:] = V*np.cos(np.radians(theta0)) # store current pixel type output

        # Create table to store output of all pixels
        Ix = np.zeros((len(wvl),ngeos))
        Qx = np.zeros((len(wvl),ngeos))
        Ux = np.zeros((len(wvl),ngeos))
        Vx = np.zeros((len(wvl),ngeos))

        # ====================
        # Loop on wavelength
        # ====================

        #for j,w in enumerate(wvl):

            # ==================
            # loop on iterations
            # ==================

        for citer in np.arange(niter):

            if citer>0 or npix!=npix2:
                picture_full = None
                # Generate a new pixel mask
                picture, mask, picture_full, ncloud, asym = mask_planet(alpha=alph, npix=npix2,
                                                                        fixed_cover=picture_full,
                                                                        cusp=cusp,
                                                                        thresh_lat=thresh_lat,
                                                                        bands=bands,
                                                                        bands_lats=bands_lats,
                                                                        patchy=patchy,
                                                                        xscale=xscale,
                                                                        yscale=yscale,
                                                                        fclouds=fclouds,
                                                                        constant_fcloud=constant_fcloud,
                                                                        full_disk=full_disk,
                                                                        sscloud=sscloud,
                                                                        sigma_c=sigma_c,
                                                                        delta_c=delta_c[a])


            #===============
            # loop on pixels types
            #===============

            if len(mask) !=0: # if there is some visible pixels
                for pixtype,model in enumerate(models):
                    IB = Is[pixtype,:,:]
                    IB = IB[:,mask==pixtype]
                    QB = Qs[pixtype,:,:]
                    QB = QB[:,mask==pixtype]
                    UB = Us[pixtype,:,:]
                    UB = UB[:,mask==pixtype]
                    VB = Vs[pixtype,:,:]
                    VB = VB[:,mask==pixtype]
                    theta0B = theta0[mask==pixtype]

                    # save some information in the model
                    vecfcloud[a,:] = fclouds
                    vecasym[a,citer] = asym
                    model.picture = np.copy(picture_full)

                    Ix[:,mask==pixtype] = IB
                    Qx[:,mask==pixtype] = QB
                    Ux[:,mask==pixtype] = UB
                    Vx[:,mask==pixtype] = VB

                #===============
                # end of loop on pixels types
                #===============

                # Integrating over planet
                Iall[:,a,citer] = np.nansum(Ix,axis=1)*apix
                Qall[:,a,citer] = np.nansum(Qx,axis=1)*apix
                Uall[:,a,citer] = np.nansum(Ux,axis=1)*apix
                Vall[:,a,citer] = np.nansum(Vx,axis=1)*apix

            else:
                # save some information in the model
                vecfcloud[a,:] = fclouds
                vecasym[a,citer] = asym
                model.picture = np.copy(picture_full)

                # Integrating over planet
                Iall[:,a,citer] = np.nan
                Qall[:,a,citer] = np.nan
                Uall[:,a,citer] = np.nan
                Vall[:,a,citer] = np.nan



            # ==================
            # end of loop on iterations
            # ==================


        # ====================
        # end of wvl loop
        # ====================

        progress = (a+1)*(citer+1)*(pixtype+1) #for the progress bar
        sys.stdout.write('\r{:2f}% done\n'.format(100.*progress/Nsteps))
        sys.stdout.flush()

    # ===================
    # end of loop on phase angle
    # ===================

    # Compute mean value
    It = Iall.mean(axis=2)
    Qt = Qall.mean(axis=2)
    Ut = Uall.mean(axis=2)
    Vt = Vall.mean(axis=2)

    # store result in first input model
    atm_model.I = It
    atm_model.Q = Qt
    atm_model.U = Ut
    atm_model.V = Vt
    atm_model.P = (-Qt/It)
    atm_model.Pl = np.sqrt( (Qt**2+Ut**2)/It**2 )
    atm_model.Pt = np.sqrt( (Qt**2+Ut**2+Vt**2)/It**2 )
    atm_model.Pu = Ut/It
    atm_model.Pv = Vt/It

    # store global results in first input model
    atm_model.Iall = Iall
    atm_model.Qall = Qall
    atm_model.Uall = Uall
    atm_model.Vall = Vall
    atm_model.Pqall = (-Qall/Iall)
    atm_model.Plall = np.sqrt( (Qall**2+Uall**2)/Iall**2 )
    atm_model.Ptall = np.sqrt( (Qall**2+Uall**2+Vall**2)/Iall**2 )
    atm_model.Puall = Uall/Iall
    atm_model.Pvall = Vall/Iall

    # saving dispersion
    atm_model.Pqstd = np.std(atm_model.Pqall, axis=2)
    atm_model.Pustd = np.std(atm_model.Puall, axis=2)
    atm_model.Pvstd = np.std(atm_model.Pvall, axis=2)
    atm_model.Plstd = np.std(atm_model.Plall, axis=2)
    atm_model.Ptstd = np.std(atm_model.Ptall, axis=2)
    atm_model.Istd = np.std(atm_model.Iall, axis=2)
    atm_model.Pqmin1s = atm_model.P-1*atm_model.Pqstd
    atm_model.Pqmin2s = atm_model.P-2*atm_model.Pqstd
    atm_model.Pqmin3s = atm_model.P-3*atm_model.Pqstd
    atm_model.Pumin1s = atm_model.Pu-1*atm_model.Pustd
    atm_model.Pumin2s = atm_model.Pu-2*atm_model.Pustd
    atm_model.Pumin3s = atm_model.Pu-3*atm_model.Pustd
    atm_model.Plmin1s = atm_model.Pl-1*atm_model.Plstd
    atm_model.Plmin2s = atm_model.Pl-2*atm_model.Plstd
    atm_model.Plmin3s = atm_model.Pl-3*atm_model.Plstd
    atm_model.Ptmin1s = atm_model.Pt-1*atm_model.Ptstd
    atm_model.Ptmin2s = atm_model.Pt-2*atm_model.Ptstd
    atm_model.Ptmin3s = atm_model.Pt-3*atm_model.Ptstd
    atm_model.Imin1s = atm_model.I-1*atm_model.Istd
    atm_model.Imin2s = atm_model.I-2*atm_model.Istd
    atm_model.Imin3s = atm_model.I-3*atm_model.Istd

    atm_model.Pqmax1s = atm_model.P+1*atm_model.Pqstd
    atm_model.Pqmax2s = atm_model.P+2*atm_model.Pqstd
    atm_model.Pqmax3s = atm_model.P+3*atm_model.Pqstd
    atm_model.Pumax1s = atm_model.Pu+1*atm_model.Pustd
    atm_model.Pumax2s = atm_model.Pu+2*atm_model.Pustd
    atm_model.Pumax3s = atm_model.Pu+3*atm_model.Pustd
    atm_model.Plmax1s = atm_model.Pl+1*atm_model.Plstd
    atm_model.Plmax2s = atm_model.Pl+2*atm_model.Plstd
    atm_model.Plmax3s = atm_model.Pl+3*atm_model.Plstd
    atm_model.Ptmax1s = atm_model.Pt+1*atm_model.Ptstd
    atm_model.Ptmax2s = atm_model.Pt+2*atm_model.Ptstd
    atm_model.Ptmax3s = atm_model.Pt+3*atm_model.Ptstd
    atm_model.Imax1s = atm_model.I+1*atm_model.Istd
    atm_model.Imax2s = atm_model.I+2*atm_model.Istd
    atm_model.Imax3s = atm_model.I+3*atm_model.Istd

    atm_model.Pqmin = atm_model.Pqall.min(axis=2)
    atm_model.Pqmax = atm_model.Pqall.max(axis=2)
    atm_model.Pumax = atm_model.Puall.max(axis=2)
    atm_model.Pumin = atm_model.Puall.min(axis=2)
    atm_model.Plmin = atm_model.Plall.min(axis=2)
    atm_model.Plmax = atm_model.Plall.max(axis=2)
    atm_model.Ptmin = atm_model.Ptall.min(axis=2)
    atm_model.Ptmax = atm_model.Ptall.max(axis=2)
    atm_model.Imin = atm_model.Iall.min(axis=2)
    atm_model.Imax = atm_model.Iall.max(axis=2)

    # Compute adjusted radiance
    B = sunblackbody(np.array(atm_model.wvl_list)*1e-6,Ts=atm_model.Ts)
    Rs = 696342000.
    Dvs = 108208930000.0
    omegas = np.pi * (Rs/Dvs)**2
    atm_model.I2 = atm_model.I * B[:,np.newaxis] * omegas

    atm_model.geom.phase = phases
    atm_model.phase = phases
    atm_model.fcloud = vecfcloud
    atm_model.asym = vecasym
    atm_model.npix = npix


def mask_planet(alpha=0, npix=20, cusp=False, thresh_lat=50., patchy=True,
                 full_disk=False, fclouds=[0.5,0.5], fixed_cover=None,
                 constant_fcloud=False, sscloud=False, sigma_c=10.,
                 delta_c=0., xscale=0.1, yscale=0.01,
                bands=False, bands_lats=[-90,90]):
    """ Generates a mask that can be used for inhomogeneous planets

    Parameters
    ----------
    alpha : int,optional
        phase angle at which the calculation is made
        default is 0
    npix : int, optional
        number of pixels on each axis
        default is 20
    cusp : bool, optional
        if True, polar cusps are added and pixels above thresh_lat have value 0
        default is False
    thresh_lat : float, optional
        latitude threshold above which the cusps extend
        default is 50.
    patchy : bool
        if True, generates a random patchy cloud cover. First type of patches
        is given value 0, second is 1, etc.
        Default is True
    bands : bool
        If True, generates bands that are defined by their latitudes. First
        band has mask value 0, second is 1, etc.
        default is False
    bands_lats : array
        array with limits of the bands. Should start at -90d and end at 90d. Must be in
        increasing order. Default is [-90,90]
    fclouds : array
        List of fractions of the planet that should be covered with pixels of
        each model
        Default is [0.5, 0.5]
    fixed_cover : None or array
        if None, a new cloud cover is generated. If a table is
        given, it will be used as the cloud cover. Default is None.
    constant_fcloud : Bool
        if True, the cloud cover fraction is calculated for
        the given phase angle and not for the whole disk
        Default is False
    sscloud : bool
        if True, a subsolar cloud is generated
    sigma_c : float
        extend in degrees of the subsolar cloud. Points between the
        subsolar point and the points with SZA=alpha+sigma are given value 0.
    delta_c : float
        longitudinal offset for the subsolar cloud, in degrees
    xscale : float
        for patchy clouds gives the typical size on x-axis, as a function of npix
    yscale : float
        for patchy clouds gives the typical size on y-axis, as a function of npix

    Returns
    -------
    grid_lit : 2d array
        array corresponding to the points of the generated pattern that are lit
    grid_out: 1d array
        flat array with the pixels that are actually lit and on the planet
    gird_full : 2d array
        mask of the whole disk, including the non-lit part of the planet
    nb_cloud : float
        fraction of the planet covered with clouds
    asym : float
        asymetry parameter: amount of pixels of the mask that don't match their
        image by symmetry through the equatorial axis

    """

    # if no specific pattern, assume full_disk
    if patchy is False and cusp is False and sscloud is False and bands is False:
        full_disk=True

    # read the pixel geometries
    ngeos, apix, theta0, theta, phi, beta, lats, longs, xs, ys = geos.getgeos(alpha, npix)

    # prepare grids
    #grid = np.zeros((npix,npix))
    grid_lit = np.zeros((npix,npix))
    grid_full = np.zeros((npix,npix))
    grid_lit[:] = np.nan

    # get angles
    theta0 = theta0[:ngeos]
    theta = theta[:ngeos]
    phi = phi[:ngeos]
    beta = beta[:ngeos]
    lats = lats[:ngeos]
    longs = longs[:ngeos]
    phase = np.ones(ngeos)*alpha
    xs = xs[:ngeos]
    ys = ys[:ngeos]
    xs = xs.round(2)
    ys = ys.round(2)

    # X and Y axis
    step = 2./npix
    X = -1 + 0.5*step + np.arange(0,npix)*step
    Y = -1 + 0.5*step + np.arange(0,npix)*step
    X = X.round(2)
    Y = Y.round(2)
    xv, yv = np.meshgrid(X,Y)
    # remove outside of disk
    grid_full[xv*xv+yv*yv>1]=np.nan
    #grid_lit[xv*xv+yv*yv>1]=np.nan

    # find where correct pixels are on a square grid
    xidx = [np.where(X==item)[0][0] for i, item in enumerate(xs) if item in X]
    yidx = [np.where(Y==item)[0][0] for i, item in enumerate(ys) if item in Y]

    if full_disk is True:
        # remove outside of disk
        grid_full[xv*xv+yv*yv>1]=np.nan
        # validate pixels lit
        grid_lit[yidx,xidx] = 0. # validate those
        grid_full[yidx,xidx] = 0. # validate those
        # wARNING! arrays have shape (nlines, ncols), hence the grid[y,x]!


    if sscloud is True:
        grid_full[:] = 1.
        # validate pixels lit
        grid_lit[yidx,xidx] = 1. # validate those

        # coordinates of subsolar point + offset
        ssidx = np.where((theta0+delta_c)<sigma_c)[0]
        newxs = xs[ssidx]
        newys = ys[ssidx]

        ssxidx = [np.where(X==item)[0][0] for i, item in enumerate(newxs) if item in X]
        ssyidx = [np.where(Y==item)[0][0] for i, item in enumerate(newys) if item in Y]
        grid_full[ssyidx,ssxidx] = 0. # validate those
        grid_lit[ssyidx,ssxidx] = 0. # validate those
        # wARNING! arrays have shape (nlines, ncols), hence the grid[y,x]!

    # If polar cusps
    if cusp is True:
        grid_full[:] = 1.
        # validate pixels lit
        grid_lit[yidx,xidx] = 1. # validate those

        # coordinates of subsolar point + offset
        cuspidx = np.where(abs(lats)>thresh_lat)[0]
        newxs = xs[cuspidx]
        newys = ys[cuspidx]

        cuspxidx = [np.where(X==item)[0][0] for i, item in enumerate(newxs) if item in X]
        cuspyidx = [np.where(Y==item)[0][0] for i, item in enumerate(newys) if item in Y]
        #grid[cuspxidx,cuspyidx] = 0. # validate those
        grid_lit[cuspyidx,cuspxidx] = 0. # validate those
        grid_full[cuspyidx,cuspxidx] = 0. # validate those
        # wARNING! arrays have shape (nlines, ncols), hence the grid[y,x]!

    if bands is True:
        grid_full[:] = 1.
        # validate pixels lit
        grid_lit[yidx,xidx] = 1. # validate those

        for latidx,lat in enumerate(bands_lats[:-1]):
            lower_bound = bands_lats[latidx]
            upper_bound = bands_lats[latidx+1]

            bandidx = np.where((lats>lower_bound)*(lats<upper_bound))[0]
            newxs = xs[bandidx]
            newys = ys[bandidx]
            bandxidx = [np.where(X==item)[0][0] for i, item in enumerate(newxs) if item in X]
            bandyidx = [np.where(Y==item)[0][0] for i, item in enumerate(newys) if item in Y]
            grid_lit[bandyidx,bandxidx] = latidx # validate those
            grid_full[bandyidx,bandxidx] = latidx # validate those

        # wARNING! arrays have shape (nlines, ncols), hence the grid[y,x]!

    if patchy is True:
        #if no fixed cover is wanted
        # n types
        ntypes = len(fclouds)
        fclouds = np.array(fclouds).astype(float) #avoids issues if integers are given
        fclouds = fclouds/sum(fclouds) #renormalization

        if fixed_cover is None:
            #compute a new one
            #starting from a no-type cover (repr. with -1)

            grid_full[:] = -1.
            # validate pixels lit
            grid_lit[yidx,xidx] = -1. # validate those

            #loop on types
            total_fcloud = 0. #total cloud coverage

            for T in np.arange(ntypes):
                total_fcloud += fclouds[T]
                nb_cloud=0

                #fill the planet with pixels
                while nb_cloud<total_fcloud:
                    # generate several multivariate gaussians on the grid
                    moy = (npr.randint(0,npix),npr.randint(0,npix))
                    cov = np.diag([npix*yscale,npix*xscale])
                    x,y = npr.multivariate_normal(moy,cov,50).T
                    # Warning: here x is N/S axis and y E/W axis
                    x = np.round(x)
                    y = np.round(y)
                    x = x.astype('int')
                    y = y.astype('int')
                    # if they go beyond the grid, wrap them around
                    x[x>=npix] = x[x>=npix]-npix
                    y[y>=npix] = y[y>=npix]-npix
                    x[x<-npix] = x[x<-npix]+npix
                    y[y<-npix] = y[y<-npix]+npix
                    # if a pixel is not already taken, give the value of the current type
                    #grid[x,y] = np.where(grid[x,y]==-1, T, grid[x,y])
                    grid_full[x,y] = np.where(grid_full[x,y]==-1, T, grid_full[x,y])
                    grid_lit[x,y] = np.where(grid_lit[x,y]==-1, T, grid_lit[x,y])
                    # wARNING! arrays have shape (nlines, ncols), hence the
                    # grid[x,y]!

                    # lit part of the planet and remove out-of-planet pixels
                    grid_full[xv*xv+yv*yv>1]=np.nan

                    # get current cloud coverage at given phase angle
                    if constant_fcloud is True:
                        cl = np.where(grid_lit>=0)[0].size
                        lit = np.where(~np.isnan(grid_lit))[0].size
                        nb_cloud = float(cl)/(lit)
                    else:
                        cl = np.where(grid_full>=0)[0].size
                        ondisk = np.where(~np.isnan(grid_full))[0].size
                        nb_cloud = float(cl)/(ondisk)

        else:
            # else take existing one
            grid_lit = np.zeros((npix,npix))
            grid_full = np.zeros((npix,npix))
            grid_full[:,:] = np.nan
            grid_lit[:,:] = np.nan
            grid_lit[yidx,xidx] = fixed_cover[yidx,xidx]
            grid_full[yidx,xidx] = fixed_cover[yidx,xidx]
            grid_lit[xv*xv+yv*yv>1]=np.nan
            grid_full[xv*xv+yv*yv>1]=np.nan

    # get current cloud coverage at given phase angle
    cl = np.where(grid_lit>=0)[0].size
    lit = np.where(~np.isnan(grid_lit))[0].size
    nb_cloud = float(cl)/(lit+1) #+1 to avoid division by 0

    #compute asymmetry factor
    #difference north south hemispheres
    N = int(max(npix/2, (npix+1)/2)) #safety measure to avoid issues with odd npix
    n = int(min(npix/2, (npix+1)/2))
    diffgrid = grid_lit[:N,:] - (grid_lit[n:,:])[::-1,:]
    # where is the difference in cloud cover?
    asympix = np.where(diffgrid[~np.isnan(diffgrid)]!=0)[0]
    # count the asymetry
    if np.size(diffgrid[~np.isnan(diffgrid)])==0:
        asym=0
    else:
        asym = np.size(asympix)/np.float(np.size(diffgrid[~np.isnan(diffgrid)]))

    # Flatten the grid with only lit points
    grid_out = grid_lit[~np.isnan(grid_lit)]
    grid_out = grid_out.flatten()

    return grid_lit, grid_out, grid_full, nb_cloud, asym


def fourier_matrix(nmug=20, surf_mat=np.diag([1,0,0,0]), nmat=4, nmuMAX=201, nmatMAX=4):
    """ A function to Fourier develop a scattering matrix for a surface
    """

    # Getting the Gauss points
    xs, w = dap.gauleg(201,nmug,0.,1.)
    smf = np.sqrt(2*xs*w)
    # adding extra nadir direction
    xs[nmug] = 1.0
    w[nmug] = 1.0
    smf[nmug] = 1.0
    nmu = nmug+1

    #mu = xs
    #mup = xs
    L = np.zeros((nmat,nmat,nmu,nmu), order='F')

    # if nmat<4, we take the corresponding submatrix of the input surface
    # matrix
    L[0:,0:,:,:] = surf_mat[:nmat,:nmat,np.newaxis,np.newaxis]
    #Bmp = np.zeros((nmat,nmat,nphi,nm))
    #Bmm = np.zeros((nmat,nmat,nphi,nm))

    #for im,mm in enumerate(m):
    #    for ip,pp in enumerate(phi):
    #        Bmp[0:,0:,ip,im] = np.diag([np.cos(mm*pp),np.cos(mm*pp),np.sin(mm*pp),np.sin(mm*pp)])
    #        Bmm[0:,0:,ip,im] = np.diag([-np.sin(mm*pp),-np.sin(mm*pp),np.cos(mm*pp),np.cos(mm*pp)])

    #Bm = Bmp + Bmm

    #Lm = np.einsum('jcbm,ijklp->ijklpm',Bm,L)

    #L2 = scpinteg.simps(Lm,phi,axis=4)
    #L2 = L2/(phi[-1]-phi[0])

    Lfin = np.zeros((nmatMAX*nmuMAX,nmatMAX*nmuMAX),order='F')
    for k in np.arange(nmat):
        for l in np.arange(nmat):
            for i in np.arange(nmu):
                for j in np.arange(nmu):
                    Lfin[nmat*i+l,nmat*j+k] = smf[i]*L[l,k,i,j]*smf[j]

    return xs,smf,Lfin


def surface_check(model,nmug, nmat=4, nmuMAX=201):
    """ A function to check what type of surface is defined by the user, and
    act accordingly for the DAP code"""

    if type(model.surface)==np.ndarray:
        mus, smf, Lfin = fourier_matrix(nmug=nmug, surf_mat=model.surface, nmat=nmat, nmuMAX=nmuMAX)
    elif model.surface==str:
        print('read fourier file for surface')

    return Lfin


def orthographic_projection(center=np.array([0,0]), npix=20, input_img='./earth_contour.png'):
    """ function to compute the orthographic proj given coordinates"""

    img = Image.open(input_img)
    img.thumbnail((4*npix,2*npix))
    maps = np.array(img)
    maps = maps/np.max(maps)
    maps[maps>0.5] = 1.0
    maps[maps<0.5] = 0.0
    maps = maps.astype('float')
    nlat, nlon = np.shape(maps)
    latitudes = np.linspace(-90,90,nlat)
    longitudes = np.linspace(-180,180,nlon)
    maps2 = np.copy(maps)
    lats = np.radians(latitudes)
    lons = np.radians(longitudes)
    center = np.radians(center)
    center[0] = -center[0]
    x = np.cos(lats[:,np.newaxis])*np.sin(lons[np.newaxis,:]-center[1])
    y = (np.cos(center[0])*np.sin(lats[:,np.newaxis]) -
         np.sin(center[0])*np.cos(lats[:,np.newaxis])*np.cos(lons[np.newaxis,:]-center[1]))
    cosc = (np.sin(center[0])*np.sin(lats[:,np.newaxis]) +
            np.cos(center[0])*np.cos(lats[:,np.newaxis])*np.cos(lons[np.newaxis,:]-center[1]))
    x[cosc<0.] = np.nan
    y[cosc<0.] = np.nan
    maps[cosc<0.] = np.nan

    # X and Y axis
    step = 2./npix
    X = -1 + 0.5*step + np.arange(0,npix)*step
    Y = -1 + 0.5*step + np.arange(0,npix)*step
    X = X.round(2)
    Y = Y.round(2)

    # prepare grids
    grid_lit = np.zeros((npix,npix))
    grid_full = np.zeros((npix,npix))
    grid_lit[:] = np.nan
    grid_full[:] = np.nan

    # find where correct pixels are on a square grid
    xidx = [np.argmin(abs(X-item)) for i, item in enumerate(x.flatten()) if ~np.isnan(item)]
    yidx = [np.argmin(abs(Y-item)) for i, item in enumerate(y.flatten()) if ~np.isnan(item)]

    for i,ii in enumerate(x.flatten()):
        if ~np.isnan(ii):
            xtmp = np.argmin(abs(X-ii))
            jj = y.flatten()[i]
            ytmp = np.argmin(abs(Y-jj))
            grid_full[xtmp,ytmp] = maps.flatten()[i]

    return x,y,maps2, xidx, yidx, grid_full


# -----------
# MAIN
# -----------

if __name__ == '__main__':

    print('ok')
